!****************************************************************************
C Column Numbers:
C        1         2         3         4         5         6         7
C23456789012345678901234567890123456789012345678901234567890123456789012
c...  ------------------------------------------------------------------
      subroutine sdvini(statev,coords,nstatv,ncrds,noel,npt,layer,kspt)
c...  ------------------------------------------------------------------
      include 'aba_param.inc'

      dimension statev(nstatv)

      statev(1)=1.0d0    ! the_g
      statev(2)=1.0d0    ! the_e
      statev(3)=1.0d0    ! the
      statev(4)=1.0d0    ! xn_g(1)
      statev(5)=1.0d0    ! xn_g(2)
      statev(6)=1.0d0    ! xn_g(3)
      statev(7)=1.0d0    ! f1(1)
      statev(8)=1.0d0    ! f1(2)
      statev(9)=1.0d0    ! f1(3)
      statev(10)=1.0d0    ! x_coordinate in reference
      statev(11)=1.0d0    ! y_coordinate in reference
      statev(12)=1.0d0    ! z_coordinate in reference

      statev(13)=0.0d0    ! stress_r
      statev(14)=0.0d0    ! r_theta
      statev(15)=0.0d0    ! r_asix
      statev(16)=0.0d0    ! theta_r
      statev(17)=0.0d0    ! stress_theta
      statev(18)=0.0d0    ! theta_axis
      statev(19)=0.0d0    ! axis_r
      statev(20)=0.0d0    ! axis_theta
      statev(21)=0.0d0    ! stress_axis

      statev(22)=1.0d0    ! elastic strain energy
      statev(23)=1.0d0    ! Ebar_el
      statev(24)=1.0d0    ! I1_el

      statev(25)=1.0d0    ! fbar(1)
      statev(26)=1.0d0    ! fbar(2)
      statev(27)=1.0d0    ! fbar(3)

      statev(28)=1.0d0    ! ff1(1)
      statev(29)=1.0d0    ! ff1(2)
      statev(30)=1.0d0    ! ff1(3)

      statev(31)=1.0d0    ! det(Fe)

      statev(32)=1.0d0    ! (1,1) of F
      statev(33)=1.0d0    ! (1,2) of F
      statev(34)=1.0d0    ! (1,3) of F
      statev(35)=1.0d0    ! (2,1) of F
      statev(36)=1.0d0    ! (2,2) of F
      statev(37)=1.0d0    ! (2,3) of F
      statev(38)=1.0d0    ! (3,1) of F
      statev(39)=1.0d0    ! (3,2) of F
      statev(40)=1.0d0    ! (3,3) of F

      statev(41)=1.0d0    ! (1,1) of Fg
      statev(42)=1.0d0    ! (1,2) of Fg
      statev(43)=1.0d0    ! (1,3) of Fg
      statev(44)=1.0d0    ! (2,1) of Fg
      statev(45)=1.0d0    ! (2,2) of Fg
      statev(46)=1.0d0    ! (2,3) of Fg
      statev(47)=1.0d0    ! (3,1) of Fg
      statev(48)=1.0d0    ! (3,2) of Fg
      statev(49)=1.0d0    ! (3,3) of Fg

      statev(50)=1.0d0    ! (1,1) of Fe
      statev(51)=1.0d0    ! (1,2) of Fe
      statev(52)=1.0d0    ! (1,3) of Fe
      statev(53)=1.0d0    ! (2,1) of Fe
      statev(54)=1.0d0    ! (2,2) of Fe
      statev(55)=1.0d0    ! (2,3) of Fe
      statev(56)=1.0d0    ! (3,1) of Fe
      statev(57)=1.0d0    ! (3,2) of Fe
      statev(58)=1.0d0    ! (3,3) of Fe

      return
      end

c...  ------------------------------------------------------------------
      subroutine umat(stress,statev,ddsdde,sse,spd,scd,
     #rpl,ddsddt,drplde,drpldt,
     #stran,dstran,time,dtime,temp,dtemp,predef,dpred,cmname,
     #ndi,nshr,ntens,nstatv,props,nprops,coords,drot,pnewdt,
     #celent,dfgrd0,dfgrd1,noel,npt,layer,kspt,kstep,kinc)
c...  ------------------------------------------------------------------
      include 'aba_param.inc'

      character*80 cmname
      dimension stress(ntens),statev(nstatv),
     #ddsdde(ntens,ntens),ddsddt(ntens),drplde(ntens),
     #stran(ntens),dstran(ntens),time(2),predef(1),dpred(1),
     #props(nprops),coords(3),drot(3,3),dfgrd0(3,3),dfgrd1(3,3)

      call umat_area_stretch(stress,statev,ddsdde,sse,
     #                       time,dtime,coords,props,dfgrd1,
     #                       ntens,ndi,nshr,nstatv,nprops,
     #                       noel,npt,kstep,kinc)

      return
      end

c...  ------------------------------------------------------------------
      subroutine umat_area_stretch(stress,statev,ddsdde,sse,
     #                             time,dtime,coords,props,dfgrd1,
     #                             ntens,ndi,nshr,nstatv,nprops,
     #                             noel,npt,kstep,kinc)
c...  ------------------------------------------------------------------
c * " This UMAT is written for a GOH constitutive material.
c * " It implements area-stretch-driven area growth in the plane normal to xn0.
c * " It is not suitable for use with local material directions?
c * " Written by Taeksang Lee in Mar 2019
c...  ------------------------------------------------------------------

      implicit none

c...  variables to be defined
      real*8  stress(ntens), ddsdde(ntens,ntens), statev(nstatv), sse

c...  variables passed in for information
      real*8  time(2), dtime, coords(3), props(nprops), dfgrd1(3,3)
      integer ntens, ndi, nshr, nstatv, nprops, noel, npt, kstep, kinc

c...  material properties
      real*8  blk, mu, kappa, k1, k2, xn0(3), f0(3), alpha, tcr, tmax, gam

c...  local variables
      integer ii, jj, kk, ll, nitl, iii, jjj
      real*8  norm, normf, xn(3), finv(3,3), ftn0(3)
      real*8  the, theg, theg_n, detf, pi
      real*8  fe(3,3), be(6), b(6), detfe, lnJe
      real*8  kg, phig, dkg, dphig, res, dres
      real*8  fac, cg_ij(6), cg_kl(6)
      real*8  xi(6), xtol, detce, stressmat(3,3), f1(3)
      real*8  ce(6), Ebar, Ebar_el, fg(3,3), fginv(6), fginvmat(3,3)
      real*8  cinv(6), cinvmat(3,3), ceinv(6), feinv(3,3), ff0(6)
      real*8  ff1(6), ximat(3,3), xn0xn0(3,3), fgmat(3,3), ddsedde(6,6)
      real*8  ddsgdde(6,6), c(6), fbar(3), ffbar(6), cg_ijmat(3,3)
      real*8  coef1, coef2, coef3, coef4, coef5, coef6, xixi(6,6)
      real*8  ff0mat(3,3), cemat(3,3), H(6), ceinvmat(3,3)
      real*8  pk2_isc_iso(3,3), pk2_isc_aniso(3,3), pk2_vol(3,3), pk2(3,3)
      real*8  dFgdtheg(3,3), const1, const2(6), const2mat(3,3)
      real*8  tmp1(3,3), tmp2(3,3), tmp3(3,3), E, E_el
      real*8  I1, I1bar, I1_el, I1bar_el, I4, I4bar, I4_el, I4bar_el
      real*8  z0(3), r0(3), R, theta, coords_g(3), Amat(3,3), Bmat(3,3), mat1(3,3), mat2(3,3)
      real*8  pk2etmp(3,3), pk2emat(3,3), xn_tmp(3), xn0xn_tmp(3,3),fbar_e(3)


      data xi/1.d0,1.d0,1.d0,0.d0,0.d0,0.d0/
      xtol = 1.d-12
      pi = 3.1415927

c...  initialize material parameters
      blk    = props(1)   ! Bulk modulus
      mu     = props(2)   ! shear modulus
      kappa  = props(3)    ! kappa, fiber dispersion: 0 to 1/3
      k1     = props(4)   ! nonlinear constant in goh model
      k2     = props(5)   ! nonlinear constant in goh model
      xn0(1) = props(6)   ! n0[1]
      xn0(2) = props(7)  ! n0[2]
      xn0(3) = props(8)  ! n0[3]

c...  save coordinate at integration point in reference configuration

      xn0(1) = 0.d0
      xn0(2) = 0.d0
      xn0(3) = 1.d0

      if (kinc.eq.1) then
        statev(10) = coords(1) ! x
        statev(11) = coords(2) ! y
        statev(12) = 0.d0 ! z

      end if

      r0(1) = statev(10)
      r0(2) = statev(11)
      r0(3) = statev(12)

c...  make xi to matrix
      call move6to33(xi,ximat)

c...  calculate 4th order identity
      xixi(1,1) = 0.5d0*(xi(1)*xi(1) + xi(1)*xi(1))
      xixi(2,2) = 0.5d0*(xi(2)*xi(2) + xi(2)*xi(2))
      xixi(3,3) = 0.5d0*(xi(3)*xi(3) + xi(3)*xi(3))
      xixi(1,2) = 0.5d0*(xi(4)*xi(4) + xi(4)*xi(4))
      xixi(1,3) = 0.5d0*(xi(5)*xi(5) + xi(5)*xi(5))
      xixi(1,4) = 0.5d0*(xi(1)*xi(4) + xi(4)*xi(1))
      xixi(1,5) = 0.5d0*(xi(1)*xi(5) + xi(5)*xi(1))
      xixi(1,6) = 0.5d0*(xi(4)*xi(5) + xi(5)*xi(4))
      xixi(2,4) = 0.5d0*(xi(4)*xi(2) + xi(2)*xi(4))
      xixi(2,5) = 0.5d0*(xi(4)*xi(6) + xi(6)*xi(4))
      xixi(2,6) = 0.5d0*(xi(2)*xi(6) + xi(6)*xi(2))
      xixi(3,4) = 0.5d0*(xi(5)*xi(6) + xi(6)*xi(5))
      xixi(3,5) = 0.5d0*(xi(5)*xi(3) + xi(3)*xi(5))
      xixi(3,6) = 0.5d0*(xi(6)*xi(3) + xi(3)*xi(6))
      xixi(4,4) = 0.5d0*(xi(1)*xi(2) + xi(4)*xi(4))
      xixi(4,5) = 0.5d0*(xi(1)*xi(6) + xi(5)*xi(4))
      xixi(4,6) = 0.5d0*(xi(4)*xi(6) + xi(5)*xi(2))
      xixi(5,5) = 0.5d0*(xi(1)*xi(3) + xi(5)*xi(5))
      xixi(5,6) = 0.5d0*(xi(4)*xi(3) + xi(5)*xi(6))
      xixi(6,6) = 0.5d0*(xi(2)*xi(3) + xi(6)*xi(6))

c...  calculate deformed elastic normal
      xn(1) = (dfgrd1(1,1)*xn0(1)+dfgrd1(1,2)*xn0(2)+dfgrd1(1,3)*xn0(3))
      xn(2) = (dfgrd1(2,1)*xn0(1)+dfgrd1(2,2)*xn0(2)+dfgrd1(2,3)*xn0(3))
      xn(3) = (dfgrd1(3,1)*xn0(1)+dfgrd1(3,2)*xn0(2)+dfgrd1(3,3)*xn0(3))
c      norm = sqrt(xn(1)*xn(1) + xn(2)*xn(2) + xn(3)*xn(3))


c... calculate xn0 otimes xn0
      do ii = 1,3
        do jj = 1,3
          xn0xn0(ii,jj) = xn0(ii)*xn0(jj)
        end do
      end do

      z0(1) = xn0(1)
      z0(2) = xn0(2)
      z0(3) = xn0(3)

c      call cross(z0,r0,f0)

      f0(1) =  - r0(2) / sqrt(r0(1)*r0(1) + r0(2)*r0(2))
      f0(2) = r0(1) / sqrt(r0(1)*r0(1) + r0(2)*r0(2))
      f0(3) =  0.d0

c      f0(3) = 0.d0
c      f0(1) = f0(1) / sqrt(f0(1)*f0(1) + f0(2)*f0(2) + f0(3)*f0(3))
c      f0(2) = f0(2) / sqrt(f0(1)*f0(1) + f0(2)*f0(2) + f0(3)*f0(3))
c      f0(3) = f0(3) / sqrt(f0(1)*f0(1) + f0(2)*f0(2) + f0(3)*f0(3))


c      f0(1) = r0(1) / sqrt(r0(1)*r0(1) + r0(2)*r0(2) + r0(3)*r0(3))
c      f0(2) = r0(2) / sqrt(r0(1)*r0(1) + r0(2)*r0(2) + r0(3)*r0(3))
c      f0(3) = r0(3) / sqrt(r0(1)*r0(1) + r0(2)*r0(2) + r0(3)*r0(3))

c      f0(1) = 1.d0
c      f0(2) = 0.d0
c      f0(3) = 0.d0

c...  obtain state variable history
      statev(7) = f0(1)
      statev(8) = f0(2)
      statev(9) = f0(3)

c...  ff0 = f0 \otimes f0
      ff0(1) = f0(1)*f0(1)
      ff0(2) = f0(2)*f0(2)
      ff0(3) = f0(3)*f0(3)
      ff0(4) = f0(1)*f0(2)
      ff0(5) = f0(1)*f0(3)
      ff0(6) = f0(2)*f0(3)

c...  ff0mat
      ff0mat(1,1) = ff0(1)
      ff0mat(2,2) = ff0(2)
      ff0mat(3,3) = ff0(3)
      ff0mat(1,2) = ff0(4)
      ff0mat(1,3) = ff0(5)
      ff0mat(2,3) = ff0(6)
      ff0mat(2,1) = ff0(4)
      ff0mat(3,1) = ff0(5)
      ff0mat(3,2) = ff0(6)

c...  structure tensor, symmetric
      do ii = 1,6
        H(ii) = kappa*xi(ii) + (1.d0 - 3.d0*kappa)*ff0(ii)
      end do

c...  calculate determinant of deformation gradient
      detf = +dfgrd1(1,1)*(dfgrd1(2,2)*dfgrd1(3,3)-dfgrd1(2,3)*dfgrd1(3,2))
     #       -dfgrd1(1,2)*(dfgrd1(2,1)*dfgrd1(3,3)-dfgrd1(2,3)*dfgrd1(3,1))
     #       +dfgrd1(1,3)*(dfgrd1(2,1)*dfgrd1(3,2)-dfgrd1(2,2)*dfgrd1(3,1))

c...  calculate area stretch
      finv(1,1) = (+dfgrd1(2,2)*dfgrd1(3,3) - dfgrd1(2,3)*dfgrd1(3,2))/detf
      finv(1,2) = (-dfgrd1(1,2)*dfgrd1(3,3) + dfgrd1(1,3)*dfgrd1(3,2))/detf
      finv(1,3) = (+dfgrd1(1,2)*dfgrd1(2,3) - dfgrd1(1,3)*dfgrd1(2,2))/detf
      finv(2,1) = (-dfgrd1(2,1)*dfgrd1(3,3) + dfgrd1(2,3)*dfgrd1(3,1))/detf
      finv(2,2) = (+dfgrd1(1,1)*dfgrd1(3,3) - dfgrd1(1,3)*dfgrd1(3,1))/detf
      finv(2,3) = (-dfgrd1(1,1)*dfgrd1(2,3) + dfgrd1(1,3)*dfgrd1(2,1))/detf
      finv(3,1) = (+dfgrd1(2,1)*dfgrd1(3,2) - dfgrd1(2,2)*dfgrd1(3,1))/detf
      finv(3,2) = (-dfgrd1(1,1)*dfgrd1(3,2) + dfgrd1(1,2)*dfgrd1(3,1))/detf
      finv(3,3) = (+dfgrd1(1,1)*dfgrd1(2,2) - dfgrd1(1,2)*dfgrd1(2,1))/detf

      ftn0(1) = finv(1,1)*xn0(1)+finv(2,1)*xn0(2)+finv(3,1)*xn0(3)
      ftn0(2) = finv(1,2)*xn0(1)+finv(2,2)*xn0(2)+finv(3,2)*xn0(3)
      ftn0(3) = finv(1,3)*xn0(1)+finv(2,3)*xn0(2)+finv(3,3)*xn0(3)

      the = detf*sqrt(ftn0(1)*ftn0(1) + ftn0(2)*ftn0(2) + ftn0(3)*ftn0(3))

c...  obtain state variable history
      theg_n = statev(1)
      theg   = theg_n

c...  update state variables
      if (kinc > 2) then
        theg = theg + dtime/1.d0*1.d0/4.d0
     #    *sqrt(statev(10)**2.d0+statev(11)**2.d0)
c        theg = theg + dtime/1.d0*(1.d0/4.d0
c     #    -1.d0/4.d0*sqrt(statev(10)**2.d0+statev(11)**2.d0))
      end if
      statev(1) = theg
      statev(2) = the/theg
      statev(3) = the

c...  Fe
c...  calculate elastic tensor Fe = 1\sqrt(theg)*F+(1-1/sqrt(theg))*n \otimes n_0
      fe(1,1) = dfgrd1(1,1)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(1)*xn0(1)
      fe(1,2) = dfgrd1(1,2)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(1)*xn0(2)
      fe(1,3) = dfgrd1(1,3)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(1)*xn0(3)
      fe(2,1) = dfgrd1(2,1)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(2)*xn0(1)
      fe(2,2) = dfgrd1(2,2)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(2)*xn0(2)
      fe(2,3) = dfgrd1(2,3)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(2)*xn0(3)
      fe(3,1) = dfgrd1(3,1)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(3)*xn0(1)
      fe(3,2) = dfgrd1(3,2)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(3)*xn0(2)
      fe(3,3) = dfgrd1(3,3)/sqrt(theg)+(1.d0 - 1.d0/sqrt(theg))*xn(3)*xn0(3)

c...  calculate deformed fiber orientation: f1 = F*f0
      f1(1) = dfgrd1(1,1)*f0(1)
     #      + dfgrd1(1,2)*f0(2)
     #      + dfgrd1(1,3)*f0(3)
      f1(2) = dfgrd1(2,1)*f0(1)
     #      + dfgrd1(2,2)*f0(2)
     #      + dfgrd1(2,3)*f0(3)
      f1(3) = dfgrd1(3,1)*f0(1)
     #      + dfgrd1(3,2)*f0(2)
     #      + dfgrd1(3,3)*f0(3)


c...  ff1 = f1 \otimes f1
      ff1(1) = f1(1)*f1(1)
      ff1(2) = f1(2)*f1(2)
      ff1(3) = f1(3)*f1(3)
      ff1(4) = f1(1)*f1(2)
      ff1(5) = f1(1)*f1(3)
      ff1(6) = f1(2)*f1(3)

      statev(28) = ff1(1)
      statev(29) = ff1(2)
      statev(30) = ff1(3)

c     Je
c...  calculate determinant of Fe
      detfe = +fe(1,1) * (fe(2,2)*fe(3,3)-fe(2,3)*fe(3,2))
     #        -fe(1,2) * (fe(2,1)*fe(3,3)-fe(2,3)*fe(3,1))
     #        +fe(1,3) * (fe(2,1)*fe(3,2)-fe(2,2)*fe(3,1))

      lnJe = dlog(detfe)

      statev(31) = detfe

c...  Fg
c...  calculate Fg = sqrt(theg)*I + (1-sqrt(theg))*n_0 \otimes n_0
      fg(1,1) = xi(1)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(1)*xn0(1)
      fg(2,2) = xi(2)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(2)*xn0(2)
      fg(3,3) = xi(3)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(3)*xn0(3)
      fg(1,2) = xi(4)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(1)*xn0(2)
      fg(1,3) = xi(5)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(1)*xn0(3)
      fg(2,3) = xi(6)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(2)*xn0(3)
      fg(2,1) = xi(4)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(2)*xn0(1)
      fg(3,1) = xi(5)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(3)*xn0(1)
      fg(3,2) = xi(6)*sqrt(theg) + (1.d0 - sqrt(theg))*xn0(3)*xn0(2)

      statev(4) = fg(1,1) * xn0(1) 
      statev(5) = fg(2,2) * xn0(2) 
      statev(6) = fg(3,3) * xn0(3) 

      statev(4) = statev(4)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0
      statev(5) = statev(5)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0
      statev(6) = statev(6)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0

      statev(32)=dfgrd1(1,1)
      statev(33)=dfgrd1(1,2)
      statev(34)=dfgrd1(1,3)
      statev(35)=dfgrd1(2,1)
      statev(36)=dfgrd1(2,2)
      statev(37)=dfgrd1(2,3)
      statev(38)=dfgrd1(3,1)
      statev(39)=dfgrd1(3,2)
      statev(40)=dfgrd1(3,3)

      statev(41)=fg(1,1)
      statev(42)=fg(1,2)
      statev(43)=fg(1,3)
      statev(44)=fg(2,1)
      statev(45)=fg(2,2)
      statev(46)=fg(2,3)
      statev(47)=fg(3,1)
      statev(48)=fg(3,2)
      statev(49)=fg(3,3)

      statev(50)=fe(1,1)
      statev(51)=fe(1,2)
      statev(52)=fe(1,3)
      statev(53)=fe(2,1)
      statev(54)=fe(2,2)
      statev(55)=fe(2,3)
      statev(56)=fe(3,1)
      statev(57)=fe(3,2)
      statev(58)=fe(3,3)

c... make Fg to matrix foam
c      call move6to33(fg,fgmat)

c     fiber orientation in the intermediate configuration
c      fbar(1) = fg(1,1)*f0(1) + fg(1,2)*f0(2) + fg(1,3)*f0(3)
c      fbar(2) = fg(2,1)*f0(1) + fg(2,2)*f0(2) + fg(2,3)*f0(3)
c      fbar(3) = fg(3,1)*f0(1) + fg(3,2)*f0(2) + fg(3,3)*f0(3)

      fbar(1) = f0(1)
      fbar(2) = f0(2)
      fbar(3) = f0(3)

c      fbar(1) = fbar(1) / sqrt(fbar(1)**2.d0 + fbar(2)**2.d0 + fbar(3)**2.d0)
c      fbar(2) = fbar(2) / sqrt(fbar(1)**2.d0 + fbar(2)**2.d0 + fbar(3)**2.d0)
c      fbar(3) = fbar(3) / sqrt(fbar(1)**2.d0 + fbar(2)**2.d0 + fbar(3)**2.d0)

      fbar_e(1) = fe(1,1)*fbar(1) + fe(1,2)*fbar(2) + fe(1,3)*fbar(3)
      fbar_e(2) = fe(2,1)*fbar(1) + fe(2,2)*fbar(2) + fe(2,3)*fbar(3)
      fbar_e(3) = fe(3,1)*fbar(1) + fe(3,2)*fbar(2) + fe(3,3)*fbar(3)

c...  ffbar = fbar \otimes fbar
      ffbar(1) = fbar(1)*fbar(1)
      ffbar(2) = fbar(2)*fbar(2)
      ffbar(3) = fbar(3)*fbar(3)
      ffbar(4) = fbar(1)*fbar(2)
      ffbar(5) = fbar(1)*fbar(3)
      ffbar(6) = fbar(2)*fbar(3)

      statev(25) = fbar(2)-f0(2)
      statev(26) = fbar(2)
      statev(27) = f0(2)

c     Fg^-1
c...  calculate Fg^-1 = 1\sqrt(theg)*I + (1-1/sqrt(theg))*n_0 \otimes n_0
      fginv(1) = xi(1)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn0(1)*xn0(1)
      fginv(2) = xi(2)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn0(2)*xn0(2)
      fginv(3) = xi(3)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn0(3)*xn0(3)
      fginv(4) = xi(4)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn0(1)*xn0(2)
      fginv(5) = xi(5)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn0(1)*xn0(3)
      fginv(6) = xi(6)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn0(2)*xn0(3)

c... make Fg^-1 to matrix form
      fginvmat(1,1) = fginv(1)
      fginvmat(2,2) = fginv(2)
      fginvmat(3,3) = fginv(3)
      fginvmat(1,2) = fginv(4)
      fginvmat(1,3) = fginv(5)
      fginvmat(2,3) = fginv(6)
      fginvmat(2,1) = fginv(4)
      fginvmat(3,1) = fginv(5)
      fginvmat(3,2) = fginv(6)

c     C^-1
c...  calculate inverse of total right cauchy-green deformation tensor
      cinv(1) = finv(1,1)*finv(1,1)+finv(1,2)*finv(1,2)+finv(1,3)*finv(1,3)
      cinv(2) = finv(2,1)*finv(2,1)+finv(2,2)*finv(2,2)+finv(2,3)*finv(2,3)
      cinv(3) = finv(3,1)*finv(3,1)+finv(3,2)*finv(3,2)+finv(3,3)*finv(3,3)
      cinv(4) = finv(1,1)*finv(2,1)+finv(1,2)*finv(2,2)+finv(1,3)*finv(2,3)
      cinv(5) = finv(1,1)*finv(3,1)+finv(1,2)*finv(3,2)+finv(1,3)*finv(3,3)
      cinv(6) = finv(2,1)*finv(3,1)+finv(2,2)*finv(3,2)+finv(2,3)*finv(3,3)

      call move6to33(cinv,cinvmat)

c...  Ce
c...  calculate elastic right cauchy-green deformation tensor ce = fe^t * fe
      ce(1) = fe(1,1)*fe(1,1) + fe(2,1)*fe(2,1) + fe(3,1)*fe(3,1)
      ce(2) = fe(1,2)*fe(1,2) + fe(2,2)*fe(2,2) + fe(3,2)*fe(3,2)
      ce(3) = fe(1,3)*fe(1,3) + fe(2,3)*fe(2,3) + fe(3,3)*fe(3,3)
      ce(4) = fe(1,1)*fe(2,1) + fe(2,1)*fe(2,2) + fe(3,1)*fe(2,3)
      ce(5) = fe(1,1)*fe(3,1) + fe(2,1)*fe(3,2) + fe(3,1)*fe(3,3)
      ce(6) = fe(1,2)*fe(3,1) + fe(2,2)*fe(3,2) + fe(3,2)*fe(3,3)

c.... cemat
      call move6to33(ce,cemat)

      detce = +ce(1) * (ce(2)*ce(3)-ce(6)*ce(6))
     #        -ce(4) * (ce(4)*ce(3)-ce(6)*ce(5))
     #        +ce(5) * (ce(4)*ce(6)-ce(2)*ce(5))

c...  C
c...  calculate right cauchy-green deformation tensor c = f^t * f
      c(1) = dfgrd1(1,1)*dfgrd1(1,1)+dfgrd1(2,1)*dfgrd1(2,1)
     #       +dfgrd1(3,1)*dfgrd1(3,1)
      c(2) = dfgrd1(1,2)*dfgrd1(1,2)+dfgrd1(2,2)*dfgrd1(2,2)
     #       +dfgrd1(3,2)*dfgrd1(3,2)
      c(3) = dfgrd1(1,3)*dfgrd1(1,3)+dfgrd1(2,3)*dfgrd1(2,3)
     #        +dfgrd1(3,3)*dfgrd1(3,3)
      c(4) = +dfgrd1(1,1)*dfgrd1(1,2)
     #       +dfgrd1(2,1)*dfgrd1(2,2)
     #       +dfgrd1(3,1)*dfgrd1(3,2)
      c(5) = dfgrd1(1,1)*dfgrd1(1,3)
     #      +dfgrd1(2,1)*dfgrd1(2,3)
     #      +dfgrd1(3,1)*dfgrd1(3,3)
      c(6) = dfgrd1(1,2)*dfgrd1(1,3)
     #      +dfgrd1(2,2)*dfgrd1(2,3)
     #      +dfgrd1(3,2)*dfgrd1(3,3)

c...  calculate elastic left cauchy-green deformation tensor be = fe * fe^t
      be(1) = fe(1,1)*fe(1,1) + fe(1,2)*fe(1,2) + fe(1,3)*fe(1,3)
      be(2) = fe(2,1)*fe(2,1) + fe(2,2)*fe(2,2) + fe(2,3)*fe(2,3)
      be(3) = fe(3,1)*fe(3,1) + fe(3,2)*fe(3,2) + fe(3,3)*fe(3,3)
      be(4) = fe(1,1)*fe(2,1) + fe(1,2)*fe(2,2) + fe(1,3)*fe(2,3)
      be(5) = fe(1,1)*fe(3,1) + fe(1,2)*fe(3,2) + fe(1,3)*fe(3,3)
      be(6) = fe(2,1)*fe(3,1) + fe(2,2)*fe(3,2) + fe(2,3)*fe(3,3)

c...  calculate left cauchy-green deformation tensor b = f * f^t
      b(1) = dfgrd1(1,1)*dfgrd1(1,1)
     #     + dfgrd1(1,2)*dfgrd1(1,2)
     #     + dfgrd1(1,3)*dfgrd1(1,3)
      b(2) = dfgrd1(2,1)*dfgrd1(2,1)
     #     + dfgrd1(2,2)*dfgrd1(2,2)
     #     + dfgrd1(2,3)*dfgrd1(2,3)
      b(3) = dfgrd1(3,1)*dfgrd1(3,1)
     #     + dfgrd1(3,2)*dfgrd1(3,2)
     #     + dfgrd1(3,3)*dfgrd1(3,3)
      b(4) = dfgrd1(1,1)*dfgrd1(2,1)
     #     + dfgrd1(1,2)*dfgrd1(2,2)
     #     + dfgrd1(1,3)*dfgrd1(2,3)
      b(5) = dfgrd1(1,1)*dfgrd1(3,1)
     #     + dfgrd1(1,2)*dfgrd1(3,2)
     #     + dfgrd1(1,3)*dfgrd1(3,3)
      b(6) = dfgrd1(2,1)*dfgrd1(3,1)
     #     + dfgrd1(2,2)*dfgrd1(3,2)
     #     + dfgrd1(2,3)*dfgrd1(3,3)

c... Fe^-1
c    Fe^-1 = Fg*F^-1
      feinv = matmul(fg,finv)

c... Ce^-1 = fe^-1 * fe^-t
c... calculate inverse of elastic right cauchy-green deformation tensor

      ceinv(1) = feinv(1,1)*feinv(1,1)
     #         + feinv(1,2)*feinv(1,2)
     #         + feinv(1,3)*feinv(1,3)
      ceinv(2) = feinv(2,1)*feinv(2,1)
     #         + feinv(2,2)*feinv(2,2)
     #         + feinv(2,3)*feinv(2,3)
      ceinv(3) = feinv(3,1)*feinv(3,1)
     #         + feinv(3,2)*feinv(3,2)
     #         + feinv(3,3)*feinv(3,3)
      ceinv(4) = feinv(1,1)*feinv(2,1)
     #         + feinv(1,2)*feinv(2,2)
     #         + feinv(1,3)*feinv(2,3)
      ceinv(5) = feinv(1,1)*feinv(3,1)
     #         + feinv(1,2)*feinv(3,2)
     #         + feinv(1,3)*feinv(3,3)
      ceinv(6) = feinv(2,1)*feinv(3,1)
     #         + feinv(2,2)*feinv(3,2)
     #         + feinv(2,3)*feinv(3,3)

c...  convert viogt to full matrix of cinv
      call move6to33(ceinv,ceinvmat)

c     stretch square in the intermediate configuration: |Fe*f0|^2; strecth^2 or I4
c     I1 and I4
      I1 = c(1) + c(2) + c(3)
      I1bar = detf**(-2.d0/3.d0)*I1

      I1_el = ce(1) + ce(2) + ce(3)
      I1bar_el = detfe**(-2.d0/3.d0)*I1_el

      I4 = ff1(1) + ff1(2) + ff1(3)
      I4bar = detf**(-2.d0/3.d0)*I4

      I4_el = fbar_e(1)*fbar_e(1) +fbar_e(2)*fbar_e(2) +fbar_e(3)*fbar_e(3)
      I4bar_el = detfe**(-2.d0/3.d0)*I4_el

c...  Ebar = kappa*(I1-3.d0) + (1.d0 - 3.d0*kappa)*(I4-1.d0)
      E = kappa*(I1-3.d0) + (1.d0-3.d0*kappa)*(I4-1.d0)
      Ebar = kappa*(I1bar-3.d0) + (1.d0-3.d0*kappa)*(I4bar-1.d0)

c...  E_el = kappa*I1_el + (1-3*kappa)*I4_el
      E_el = kappa*(I1_el-3.d0) + (1.d0-3.d0*kappa)*(I4_el-1.d0)
      Ebar_el = kappa*(I1bar_el-3.d0) + (1.d0-3.d0*kappa)*(I4bar_el-1.d0)

c...  only tension can generate contribution of fiber
      if (E.lt.0) then
        E = 0.d0
      end if

      if (Ebar.lt.0) then
        Ebar = 0.d0
      end if

      if (E_el.lt.0) then
        E_el = 0.d0
      end if

      if (Ebar_el.lt.0) then
        Ebar_el = 0.d0
      end if
      statev(23) = Ebar_el
      statev(24) = I1_el
c...  calculate PK2 stress
c     pk2 = pk2_vol + pk2_isc
c         = pk2_vol + pk2_isc_iso + pk2_isc_aniso

      pk2_vol = blk/2.d0*(detfe**2.d0-1.d0)*cinvmat

      pk2_isc_iso = mu/detfe**(2.d0/3.d0)
     #            *(matmul(fginvmat,transpose(fginvmat))
     #            - I1_el/3.d0*cinvmat)

      pk2_isc_aniso = 2.d0*kappa*matmul(fginvmat,transpose(fginvmat))
     #              +2.d0*(1.d0-3.d0*kappa)
     #              *matmul(matmul(fginvmat,ff0mat),transpose(fginvmat))
     #              -2.d0/3.d0*kappa*I1_el*cinvmat
     #              -2.d0/3.d0*(1.d0-3.d0*kappa)*I4_el*cinvmat

      pk2_isc_aniso = k1*Ebar_el*exp(k2*Ebar_el**2.d0)*detfe**(-2.d0/3.d0)
     #              *pk2_isc_aniso

      pk2 = detf/detfe*(pk2_vol + pk2_isc_iso + pk2_isc_aniso)


      coef1 = blk*detfe**2.d0 + 2.d0/9.d0*mu*detfe**(-2.d0/3.d0)*I1_el
     #        +4.d0/9.d0*k1*exp(k2*Ebar_el**2.d0)*detfe**(-4.d0/3.d0)
     #          *(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)**2.d0
     #        +8.d0/9.d0*k1*k2*Ebar_el**2.d0*exp(k2*Ebar_el**2.d0)
     #        *detfe**(-4.d0/3.d0)
     #          *(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)**2.d0
     #        +4.d0/9.d0*k1*Ebar_el*exp(k2*Ebar_el**2.d0)*detfe**(-2.d0/3.d0)
     #          *(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)
      coef1 = coef1/detfe

      coef2 = -2.d0/3.d0*mu*detfe**(-2.d0/3.d0)
      coef2 = coef2/detfe

      coef3 = -blk/2.d0*(detfe**2.d0-1.d0) + mu/3.d0*detfe**(-2.d0/3.d0)*I1_el
     #        +2.d0/3.d0*k1*Ebar_el*exp(k2*Ebar_el**2.d0)*detfe**(-2.d0/3.d0)
     #          *(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)
      coef3 = coef3/detfe

      coef4 = -4.d0/3.d0*k1*exp(k2*Ebar_el**2.d0)
     #        *detfe**(-4.d0/3.d0)*(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)
     #        -8.d0/3.d0*k1*k2*Ebar_el**2.d0*exp(k2*Ebar_el**2.d0)
     #          *detfe**(-4.d0/3.d0)*(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)
     #        -4.d0/3.d0*k1*Ebar_el*exp(k2*Ebar_el**2.d0)*detfe**(-2.d0/3.d0)
      coef4 = coef4/detfe

      coef5 = 4.d0*k1*exp(k2*Ebar_el**2.d0)*detfe**(-4.d0/3.d0)
     #        +8.d0*k1*k2*Ebar_el**2.d0*exp(k2*Ebar_el**2.d0)
     #        *detfe**(-4.d0/3.d0)
      coef5 = coef5/detfe

      coef6 = -4.d0/3.d0*k1*Ebar_el*exp(k2*Ebar_el**2.d0)*detfe**(-2.d0/3.d0)
     #        -4.d0/3.d0*k1*exp(k2*Ebar_el**2.d0)*detfe**(-4.d0/3.d0)
     #          *(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)
     #        -8.d0/3.d0*k1*k2*Ebar_el**2.d0*exp(k2*Ebar_el**2.d0)
     #        *detfe**(-4.d0/3.d0)
     #          *(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el)
      coef6 = coef6/detfe


c.... when fiber does not have tension.
      if (Ebar_el.eq.0.d0) then
        coef1 = blk*detfe**2.d0 + 2.d0/9.d0*mu*detfe**(-2.d0/3.d0)*I1_el
        coef1 = coef1/detfe

        coef2 = -2.d0/3.d0*mu*detfe**(-2.d0/3.d0)
        coef2 = coef2/detfe

        coef3 = -blk/2.d0*(detfe**2.d0-1.d0) + mu/3.d0*detfe**(-2.d0/3.d0)*I1_el
        coef3 = coef3/detfe

        coef4 = 0.d0
        coef4 = coef4/detfe

        coef5 = 0.d0
        coef5 = coef5/detfe

        coef6 = 0.d0
        coef6 = coef6/detfe
      end if
c      print *, 'coef1, coef6', coef1, coef6
c...  calculate elastic tangent, ddsedde
      ddsedde(1,1) = coef1*xi(1)*xi(1)
     # + coef2*be(1)*xi(1) + coef2*xi(1)*be(1)
     #              +coef3*2.d0*xixi(1,1)
     #     +coef4*xi(1)*(kappa*be(1) + (1.d0-3.d0*kappa)*fbar(1)*fbar(1))
     #     +coef5*(kappa*be(1) + (1.d0-3.d0*kappa)*fbar(1)*fbar(1))
     #    *(kappa*be(1) + (1.d0-3.d0*kappa)*fbar(1)*fbar(1))
     #    +coef6*(kappa*be(1) + (1.d0-3.d0*kappa)*fbar(1)*fbar(1))*xi(1)

      ddsedde(2,2) = coef1*xi(2)*xi(2)
     # + coef2*be(2)*xi(2) + coef2*xi(2)*be(2)
     #              +coef3*2.d0*xixi(2,2)
     #     +coef4*xi(2)*(kappa*be(2) + (1.d0-3.d0*kappa)*fbar(2)*fbar(2))
     #   +coef5*(kappa*be(2) + (1.d0-3.d0*kappa)*fbar(2)*fbar(2))
     #   *(kappa*be(2) + (1.d0-3.d0*kappa)*fbar(2)*fbar(2))
     #   +coef6*(kappa*be(2) + (1.d0-3.d0*kappa)*fbar(2)*fbar(2))*xi(2)

      ddsedde(3,3) = coef1*xi(3)*xi(3)
     # + coef2*be(3)*xi(3) + coef2*xi(3)*be(3)
     #              +coef3*2.d0*xixi(3,3)
     #    +coef4*xi(3)*(kappa*be(3) + (1.d0-3.d0*kappa)*fbar(3)*fbar(3))
     #   +coef5*(kappa*be(3) + (1.d0-3.d0*kappa)*fbar(3)*fbar(3))
     #   *(kappa*be(3) + (1.d0-3.d0*kappa)*fbar(3)*fbar(3))
     #  +coef6*(kappa*be(3) + (1.d0-3.d0*kappa)*fbar(3)*fbar(3))*xi(3)

c     ii=1,jj=1,kk=2,ll=2; iii=1,jjj=2
      ii=1
      jj=1
      kk=2
      ll=2
      iii=1
      jjj=2
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #   +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=1,jjj=3; ii=1,jj=1,kk=3,ll=3;
      iii=1
      jjj=3
      ii=1
      jj=1
      kk=3
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #  *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     # +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=2,jjj=3; ii=2,jj=2,kk=3,ll=3;
      iii=2
      jjj=3
      ii=2
      jj=2
      kk=3
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #  *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     # +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=1,jjj=4; ii=1,jj=1,kk=1,ll=2;
      iii=1
      jjj=4
      ii=1
      jj=1
      kk=1
      ll=2
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #   +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=2,jjj=4; ii=2,jj=2,kk=1,ll=2;
      iii=2
      jjj=4
      ii=2
      jj=2
      kk=1
      ll=2
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #  *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=3,jjj=4; ii=3,jj=3,kk=1,ll=2;
      iii=3
      jjj=4
      ii=3
      jj=3
      kk=1
      ll=2
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #  *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     # +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=1,jjj=5; ii=1,jj=1,kk=1,ll=3;
      iii=1
      jjj=5
      ii=1
      jj=1
      kk=1
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #  *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=2,jjj=5; ii=2,jj=2,kk=1,ll=3;
      iii=2
      jjj=5
      ii=2
      jj=2
      kk=1
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     # +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     # +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=3,jjj=5; ii=3,jj=3,kk=1,ll=3;
      iii=3
      jjj=5
      ii=3
      jj=3
      kk=1
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #   +coef3*2.d0*xixi(iii,jjj)
     #   +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #    +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=4,jjj=5; ii=1,jj=2,kk=1,ll=3;
      iii=4
      jjj=5
      ii=1
      jj=2
      kk=1
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #  +coef3*2.d0*xixi(iii,jjj)
     #   +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=4,jjj=4; ii=1,jj=2,kk=1,ll=2;
      iii=4
      jjj=4
      ii=1
      jj=2
      kk=1
      ll=2
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=5,jjj=5; ii=1,jj=3,kk=1,ll=3;
      iii=5
      jjj=5
      ii=1
      jj=3
      kk=1
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #   +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #    +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     # +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=6,jjj=6; ii=2,jj=3,kk=2,ll=3;
      iii=6
      jjj=6
      ii=2
      jj=3
      kk=2
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     #   + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #  *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=1,jjj=6; ii=1,jj=1,kk=2,ll=3;
      iii=1
      jjj=6
      ii=1
      jj=1
      kk=2
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     # + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #   +coef3*2.d0*xixi(iii,jjj)
     #   +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #    *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=2,jjj=6; ii=2,jj=2,kk=2,ll=3;
      iii=2
      jjj=6
      ii=2
      jj=2
      kk=2
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     #     + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #    +coef3*2.d0*xixi(iii,jjj)
     #   +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #   *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=3,jjj=6; ii=3,jj=3,kk=2,ll=3;
      iii=3
      jjj=6
      ii=3
      jj=3
      kk=2
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     #     + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #  +coef3*2.d0*xixi(iii,jjj)
     #  +coef4*xi(iii)*(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #   +coef5*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #    *(kappa*be(jjj) + (1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #  +coef6*(kappa*be(iii) + (1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=4,jjj=6; ii=1,jj=2,kk=2,ll=3;
      iii=4
      jjj=6
      ii=1
      jj=2
      kk=2
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     #        + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #        +coef3*2.d0*xixi(iii,jjj)
     #     +coef4*xi(iii)*(kappa*be(jjj)+(1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #     +coef5*(kappa*be(iii)+(1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #      *(kappa*be(jjj)+(1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #    +coef6*(kappa*be(iii)+(1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)

c     iii=5,jjj=6; ii=1,jj=3,kk=2,ll=3;
      iii=5
      jjj=6
      ii=1
      jj=3
      kk=2
      ll=3
      ddsedde(iii,jjj) = coef1*xi(iii)*xi(jjj)
     #              + coef2*be(iii)*xi(jjj) + coef2*xi(iii)*be(jjj)
     #              +coef3*2.d0*xixi(iii,jjj)
     #              +coef4*xi(iii)*(kappa*be(jjj)
     #                +(1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #              +coef5*(kappa*be(iii)+(1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))
     #                *(kappa*be(jjj)+(1.d0-3.d0*kappa)*fbar(kk)*fbar(ll))
     #     +coef6*(kappa*be(iii)+(1.d0-3.d0*kappa)*fbar(ii)*fbar(jj))*xi(jjj)


c...  use symmetry to fill in the rest
      do ii = 2,ntens
        do jj = 1,ii-1
          ddsedde(ii,jj) = ddsedde(jj,ii)
        end do
      end do

c...  calculate Cauchy stress
c        stress(i) = ((lam*lnJe-mu)*xi(i) + mu*be(i))/detfe
      do ii = 1,ntens
        stress(ii) = (blk/2.d0*(detfe**2.d0-1.d0)
     #                -mu/3.d0*detfe**(-2.d0/3.d0)*I1_el
     #                -2.d0/3.d0*k1*Ebar_el*exp(k2*Ebar_el**2.d0)
     #                *detfe**(-2.d0/3.d0)
     #                *(kappa*I1_el+(1.d0-3.d0*kappa)*I4_el))*xi(ii)
     #               +(mu*detfe**(-2.d0/3.d0)
     #                +2.d0*k1*Ebar_el*exp(k2*Ebar_el**2.d0)*detfe**(-2.d0/3.d0)
     #                *kappa)*be(ii)
     #               +2.d0*k1*Ebar_el*exp(k2*Ebar_el**2.d0)*detfe**(-2.d0/3.d0)
     #                 *(1.d0-3.d0*kappa)*ffbar(ii)
        stress(ii) = stress(ii)/detfe
      end do

c... make stress to matrix foam
      call move6to33(stress,stressmat)

      xn_tmp(1) = finv(1,1)*xn0(1) + finv(2,1)*xn0(2) + finv(3,1)*xn0(3)
      xn_tmp(2) = finv(1,2)*xn0(1) + finv(2,2)*xn0(2) + finv(3,2)*xn0(3)
      xn_tmp(3) = finv(1,3)*xn0(1) + finv(2,3)*xn0(2) + finv(3,3)*xn0(3)
      call outer3by3(xn0,xn_tmp,xn0xn_tmp)
      
c      feinv = finv + (theg-1.d0) * xn0xn_tmp
      pk2etmp = matmul(feinv,stressmat)
      pk2emat = detfe * matmul(pk2etmp,transpose(feinv))

c...  for hoop direction
      coords_g(1) = fg(1,1) * r0(1) 
      coords_g(2) = fg(2,2) * r0(2)
      coords_g(3) = fg(3,3) * r0(3)

      R = sqrt(coords_g(1)**2.d0 + coords_g(2)**2.d0)

      theta = atan(abs(coords_g(2)/coords_g(1)))


      if (coords_g(1).gt.0.d0 .and. coords_g(2).gt.0.d0) then

        theta = theta

      else if (coords_g(1).gt.0.d0 .and. coords_g(2).lt.0.d0) then

        theta = 2.d0*pi - theta

      else if (coords_g(1).lt.0.d0 .and. coords_g(2).lt.0.d0) then

        theta = pi + theta

      else if (coords_g(1).lt.0.d0 .and. coords_g(2).gt.0.d0) then

        theta = pi - theta

      end if

      Amat(1,1) = cos(theta)
      Amat(1,2) = sin(theta)
      Amat(1,3) = 0.d0

      Amat(2,1) = -sin(theta)
      Amat(2,2) = cos(theta)
      Amat(2,3) = 0.d0

      Amat(3,1) = 0.d0
      Amat(3,2) = 0.d0
      Amat(3,3) = 1.d0

      Bmat(1,1) = cos(theta)
      Bmat(1,2) = -sin(theta)
      Bmat(1,3) = 0.d0

      Bmat(2,1) = sin(theta)
      Bmat(2,2) = cos(theta)
      Bmat(2,3) = 0.d0

      Bmat(3,1) = 0.d0
      Bmat(3,2) = 0.d0
      Bmat(3,3) = 1.d0

      mat1 = matmul(Amat,pk2emat)
      mat2 = matmul(mat1,Bmat)

      statev(13) = mat2(1,1) ! stress_r
      statev(14) = mat2(1,2) ! r_theta
      statev(15) = mat2(1,3) ! r_axis
      statev(16) = mat2(2,1) ! theta_r
      statev(17) = mat2(2,2) ! stress_theta
      statev(18) = mat2(2,3) ! theta_axis
      statev(19) = mat2(3,1) ! axis_r
      statev(20) = mat2(3,2) ! axis_theta
      statev(21) = mat2(3,3) ! stress_axis


c     Fg^-1:dFg/dtheg
      do ii = 1,3
        do jj = 1,3
          dFgdtheg(ii,jj) = (ximat(ii,jj)-xn0xn0(ii,jj))/2.d0/theg**0.5d0
        end do
      end do

      tmp1 = matmul(matmul(fe,dFgdtheg),matmul(pk2,transpose(dfgrd1)))
      tmp2 = matmul(matmul(dfgrd1,pk2),matmul(dFgdtheg,transpose(fe)))
      tmp3 = matmul(fe,matmul(dFgdtheg,finv))

      call doubledot(fginvmat,dFgdtheg,const1)
      call doubledot42(ddsedde,tmp3,const2)
      call move6to33(const2,const2mat)

c...  calculate growth tangent
      do ii =1,3
        do jj = 1,3
          cg_ijmat(ii,jj) = +stressmat(ii,jj)*const1
     #                   -(tmp1(ii,jj) + tmp2(ii,jj))/detf
     #                   -const2mat(ii,jj)
        end do
      end do

      call move33to6(cg_ijmat,cg_ij)

      cg_kl(1) = the*xi(1) - detf*detf/the*ftn0(1)*ftn0(1)
      cg_kl(2) = the*xi(2) - detf*detf/the*ftn0(2)*ftn0(2)
      cg_kl(3) = the*xi(3) - detf*detf/the*ftn0(3)*ftn0(3)
      cg_kl(4) = the*xi(4) - detf*detf/the*ftn0(1)*ftn0(2)
      cg_kl(5) = the*xi(5) - detf*detf/the*ftn0(1)*ftn0(3)
      cg_kl(6) = the*xi(6) - detf*detf/the*ftn0(2)*ftn0(3)

c...  geometric tangent
      do ii=1,3
        ddsgdde(ii,ii) = 2.d0*stress(ii)
      end do
      ddsgdde(1,2) = 0.d0
      ddsgdde(1,3) = 0.d0
      ddsgdde(2,3) = 0.d0

      ddsgdde(1,4) = stress(4)
      ddsgdde(1,5) = stress(5)
      ddsgdde(1,6) = 0.d0
      ddsgdde(2,4) = stress(4)
      ddsgdde(2,5) = 0.d0
      ddsgdde(2,6) = stress(6)
      ddsgdde(3,4) = 0.d0
      ddsgdde(3,5) = stress(5)
      ddsgdde(3,6) = stress(6)
      ddsgdde(4,5) = stress(6)/2.d0
      ddsgdde(4,6) = stress(5)/2.d0
      ddsgdde(5,6) = stress(4)/2.d0
      ddsgdde(4,4) = (stress(2) + stress(1))/2.d0
      ddsgdde(5,5) = (stress(3) + stress(1))/2.d0
      ddsgdde(6,6) = (stress(3) + stress(2))/2.d0

c... use symmetric condition to fill other parts of geometric tangent
      do ii = 2,ntens
        do jj = 1,ii-1
          ddsgdde(ii,jj) = ddsgdde(jj,ii)
        end do
      end do

c...  compile tangent
      do ii = 1,ntens
        do jj = 1,ntens
c          ddsdde(ii,jj)=ddsedde(ii,jj)+ddsgdde(ii,jj)
c     #                  +fac*cg_ij(ii)*cg_kl(jj)
          ddsdde(ii,jj)=ddsedde(ii,jj)+ddsgdde(ii,jj)
        end do
      end do

c...  calculate strain energy
      sse = +mu/2.d0*(I1bar_el-3.d0)
     #      +blk/2.d0*((detfe**2.d0-1.d0)/2.d0-lnJe)
     #      +0.5d0*k1/k2*(exp(k2*Ebar_el**2.d0)-1.d0)
      statev(22) = sse
      return
      end

c...  ------------------------------------------------------------------

      subroutine cross(aa, bb,cc)
      implicit none

      real*8 :: cc(3)
      real*8 :: aa(3), bb(3)

      cc(1) = aa(2) * bb(3) - aa(3) * bb(2)
      cc(2) = aa(3) * bb(1) - aa(1) * bb(3)
      cc(3) = aa(1) * bb(2) - aa(2) * bb(1)

      return
      end
c...  ------------------------------------------------------------------
c...  Subroutine for doubledot product of two 2nd order tensor
c...  ------------------------------------------------------------------
      subroutine doubledot(matA,matB,prd1)
      implicit none
      real*8   matA(3,3), matB(3,3)
      real*8   prd1

      prd1=+matA(1,1)*matB(1,1)+matA(2,2)*matB(2,2)+matA(3,3)*matB(3,3)
     #       +matA(1,2)*matB(1,2)+matA(1,3)*matB(1,3)+matA(2,3)*matB(2,3)
     #       +matA(2,1)*matB(2,1)+matA(3,1)*matB(3,1)+matA(3,2)*matB(3,2)

      return
      end

c...  ------------------------------------------------------------------
c...  Subroutine for doubledot product of symmetric 4th order and 2nd order tensor
c...  ------------------------------------------------------------------
      subroutine doubledot42(matA,matB,voigtC)
      implicit none
      real*8   matA(6,6), matB(3,3), voigtC(6)
      integer  ii

      do ii = 1,6
        voigtC(ii)=+matA(ii,1)*matB(1,1)+matA(ii,2)
     #             *matB(2,2)+matA(ii,3)*matB(3,3)
     #             +matA(ii,4)*matB(1,2)+matA(ii,5)
     #             *matB(1,3)+matA(ii,6)*matB(2,3)
     #             +matA(ii,4)*matB(2,1)+matA(ii,5)
     #             *matB(3,1)+matA(ii,6)*matB(3,2)
      end do

      return
      end

c...  ------------------------------------------------------------------
c...  Subroutine for moving 6x1 tensor to 3x3 tensor
c...  ------------------------------------------------------------------
      subroutine move6to33(a6,a33)
      implicit none
      real*8   a6(6),a33(3,3)

      a33(1,1) = a6(1)
      a33(2,2) = a6(2)
      a33(3,3) = a6(3)
      a33(1,2) = a6(4)
      a33(1,3) = a6(5)
      a33(2,3) = a6(6)
      a33(2,1) = a33(1,2)
      a33(3,1) = a33(1,3)
      a33(3,2) = a33(2,3)

      return
      end

c...  ------------------------------------------------------------------
c...  Subroutine for moving 3x3 tensor to 6x1 tensor
c...  ------------------------------------------------------------------
      subroutine move33to6(a33,a6)
      implicit none
      real*8   a6(6),a33(3,3)

      a6(1) = a33(1,1)
      a6(2) = a33(2,2)
      a6(3) = a33(3,3)
      a6(4) = a33(1,2)
      a6(5) = a33(1,3)
      a6(6) = a33(2,3)

      return
      end
c...  ------------------------------------------------------------------
c...  Subroutine for outer product 3x1 tensor by 3x1 tensor
c...  ------------------------------------------------------------------
      subroutine outer3by3(a3,b3,c33)
      implicit none
      real*8   a3(3),b3(3),c33(3,3)

      c33(1,1) = a3(1)*b3(1)
      c33(2,2) = a3(1)*b3(2)
      c33(3,3) = a3(1)*b3(3)
      c33(1,2) = a3(2)*b3(1)
      c33(1,3) = a3(2)*b3(2)
      c33(2,3) = a3(2)*b3(3)
      c33(2,1) = a3(3)*b3(1)
      c33(3,1) = a3(3)*b3(2)
      c33(3,2) = a3(3)*b3(3)

      return
      end
c...  ------------------------------------------------------------------