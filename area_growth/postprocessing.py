from abaqus import *
import sys
from odbAccess import *
import numpy as np
from abaqusConstants import *
from visualization import *
from numpy import linalg as LA
import numpy.matlib

#increments = np.linspace(10,70,7)
#increments = increments.astype(np.int32)

odbPath = 'JobName.odb'
inputfile = 'InputFile_quad_only_new.inp'
o1=session.openOdb(name=odbPath)
odb = session.odbs[odbPath]
session.viewports['Viewport: 1'].setValues(displayedObject=o1)
session.viewports['Viewport: 1'].odbDisplay.basicOptions.setValues(averageElementOutput=False)


nodes = 10452 #9387 
lin_n_node =  2808 #2502
elements =  2080 #1920
ipt = 8
increments = 11


theg = np.zeros(((elements,ipt,increments)))
normal_x = np.zeros(((elements,ipt,increments)))
normal_y = np.zeros(((elements,ipt,increments)))
normal_z = np.zeros(((elements,ipt,increments)))
node_x = np.zeros((nodes,increments))
node_y = np.zeros((nodes,increments))
node_z = np.zeros((nodes,increments))
s11 = np.zeros(((elements,ipt,increments)))
s22 = np.zeros(((elements,ipt,increments)))
s33 = np.zeros(((elements,ipt,increments)))
sse = np.zeros(((elements,ipt,increments)))
# read abaqus odb file

for i, element in enumerate(range(1,elements+1)):

	# theta_g
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV1', outputPosition=INTEGRATION_POINT)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('SDV1', INTEGRATION_POINT), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		theg_temp = session.xyDataObjects['SDV1 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(theg_temp):
			theg[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['SDV1 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]

	# x-coordinate of normal vector
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV4', outputPosition=INTEGRATION_POINT)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('SDV4', INTEGRATION_POINT), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		normal_x_temp = session.xyDataObjects['SDV4 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(normal_x_temp):
			normal_x[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['SDV4 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]


	# y-coordinate of normal vector
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV5', outputPosition=INTEGRATION_POINT)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('SDV5', INTEGRATION_POINT), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		normal_y_temp = session.xyDataObjects['SDV5 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(normal_y_temp):
			normal_y[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['SDV5 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]


	# z-coordinate of normal vector
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV6', outputPosition=INTEGRATION_POINT)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('SDV6', INTEGRATION_POINT), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		normal_z_temp = session.xyDataObjects['SDV6 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(normal_z_temp):
			normal_z[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['SDV6 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]


	# s11 component at integration point
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='S', outputPosition=INTEGRATION_POINT, refinement=(COMPONENT, 'S11'),)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('S', INTEGRATION_POINT, ((COMPONENT, 'S11'), )), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		s11_temp = session.xyDataObjects['S:S11 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(s11_temp):
			s11[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['S:S11 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]

	# s22 component at integration point
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='S', outputPosition=INTEGRATION_POINT, refinement=(COMPONENT, 'S22'),)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('S', INTEGRATION_POINT, ((COMPONENT, 'S22'), )), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		s22_temp = session.xyDataObjects['S:S22 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(s22_temp):
			s22[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['S:S22 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]

	# s33 component at integration point
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='S', outputPosition=INTEGRATION_POINT, refinement=(COMPONENT, 'S33'),)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('S', INTEGRATION_POINT, ((COMPONENT, 'S33'), )), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		s33_temp = session.xyDataObjects['S:S33 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(s33_temp):
			s33[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['S:S33 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]


	# sse
	session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV22', outputPosition=INTEGRATION_POINT)
	session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, variable=(('SDV22', INTEGRATION_POINT), ), elementLabels=(('INST-SKIN', ('%s'%element, )), ))
	
	for j in range(ipt):
		sse_temp = session.xyDataObjects['SDV22 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]
		for aa,bb in enumerate(sse_temp):
			sse[i,j,aa] = np.float(bb[-1])
		del session.xyDataObjects['SDV22 PI: INST-SKIN E: %s IP: %s'%(element,j+1)]


for i, node in enumerate(range(1,nodes+1)):

	
	session.xyDataListFromField(odb=odb, outputPosition=NODAL, variable=(('COORD', NODAL), ), nodeLabels=(('INST-SKIN', ('%s'%node, )), ))
	# node_x
	for increment in range(1,increments+1):
		node_x_temp = session.xyDataObjects['COORD:COOR1 PI: INST-SKIN N: %s'%node]
		for aa,bb in enumerate(node_x_temp):
			node_x[i,aa] = np.float(bb[-1])
	del session.xyDataObjects['COORD:COOR1 PI: INST-SKIN N: %s'%node]

	# node_y
	for increment in range(1,increments+1):
		node_y_temp = session.xyDataObjects['COORD:COOR2 PI: INST-SKIN N: %s'%node]
		for aa,bb in enumerate(node_y_temp):
			node_y[i,aa] = np.float(bb[-1])
	del session.xyDataObjects['COORD:COOR2 PI: INST-SKIN N: %s'%node]

	# node_z
	for increment in range(1,increments+1):
		node_z_temp = session.xyDataObjects['COORD:COOR3 PI: INST-SKIN N: %s'%node]
		for aa,bb in enumerate(node_z_temp):
			node_z[i,aa] = np.float(bb[-1])
	del session.xyDataObjects['COORD:COOR3 PI: INST-SKIN N: %s'%node]


session.odbs[odbPath].close()


def calc_global_lin(comp,lin_n_node):
	MM = np.zeros((lin_n_node,lin_n_node))
	FF = np.zeros((lin_n_node))

	for i in range(n_elem):
	    # assemble into global stiffness
		n1 = elements[i,0]-1
		n2 = elements[i,1]-1
		n3 = elements[i,2]-1
		n4 = elements[i,3]-1
		n5 = elements[i,4]-1
		n6 = elements[i,5]-1
		n7 = elements[i,6]-1
		n8 = elements[i,7]-1
	    # N_C3D8[a,b] means a integration point and b node
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			for jmidx,jm in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
				# at each integration point
				for jsub in range(8):
					MM[im,jm] += N_C3D8[jsub,imidx]*N_C3D8[jsub,jmidx]

		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			# at each integration point
			for jsub in range(8):
				if jsub == 2:
					FF[im] += N_C3D8[jsub,imidx]*comp[i,3,increment]
				elif jsub == 3:
					FF[im] += N_C3D8[jsub,imidx]*comp[i,2,increment]
				elif jsub == 6:
					FF[im] += N_C3D8[jsub,imidx]*comp[i,7,increment]
				elif jsub == 7:
					FF[im] += N_C3D8[jsub,imidx]*comp[i,6,increment]
				else:
					FF[im] += N_C3D8[jsub,imidx]*comp[i,jsub,increment]

	return LA.solve(MM,FF) # at the current increment

def calc_global_thetag():
	MM = np.zeros((n_node,n_node))
	FF = np.zeros((n_node))

	for i in range(n_elem):
	    # assemble into global stiffness
		n1 = elements[i,0]-1
		n2 = elements[i,1]-1
		n3 = elements[i,2]-1
		n4 = elements[i,3]-1
		n5 = elements[i,4]-1
		n6 = elements[i,5]-1
		n7 = elements[i,6]-1
		n8 = elements[i,7]-1
	    # N_C3D8[a,b] means a integration point and b node
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			for jmidx,jm in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
				# at each integration point
				for jsub in range(8):
					MM[im,jm] += N_C3D8[jsub,imidx]*N_C3D8[jsub,jmidx]

		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			# at each integration point
			for jsub in range(8):
				if jsub == 2:
					FF[im] += N_C3D8[jsub,imidx]*theg[i,3,increment]
				elif jsub == 3:
					FF[im] += N_C3D8[jsub,imidx]*theg[i,2,increment]
				elif jsub == 6:
					FF[im] += N_C3D8[jsub,imidx]*theg[i,7,increment]
				elif jsub == 7:
					FF[im] += N_C3D8[jsub,imidx]*theg[i,6,increment]
				else:
					FF[im] += N_C3D8[jsub,imidx]*theg[i,jsub,increment]

	return LA.solve(MM,FF) # at the current increment

def calc_global_stress(stress_comp):
	MM = np.zeros((n_node,n_node))
	FF = np.zeros((n_node))

	for i in range(n_elem):
	    # assemble into global stiffness
		n1 = elements[i,0]-1
		n2 = elements[i,1]-1
		n3 = elements[i,2]-1
		n4 = elements[i,3]-1
		n5 = elements[i,4]-1
		n6 = elements[i,5]-1
		n7 = elements[i,6]-1
		n8 = elements[i,7]-1
	    # N_C3D8[a,b] means a integration point and b node
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			for jmidx,jm in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
				# at each integration point
				for jsub in range(8):
					MM[im,jm] += N_C3D8[jsub,imidx]*N_C3D8[jsub,jmidx]
		
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			# at each integration point
			for jsub in range(8):
				if jsub == 2:
					FF[im] += N_C3D8[jsub,imidx]*stress_comp[i,3,increment]
				elif jsub == 3:
					FF[im] += N_C3D8[jsub,imidx]*stress_comp[i,2,increment]
				elif jsub == 6:
					FF[im] += N_C3D8[jsub,imidx]*stress_comp[i,7,increment]
				elif jsub == 7:
					FF[im] += N_C3D8[jsub,imidx]*stress_comp[i,6,increment]
				else:
					FF[im] += N_C3D8[jsub,imidx]*stress_comp[i,jsub,increment]

	return LA.solve(MM,FF) # at the current increment

def calc_global_GddotG():
	MM = np.zeros((lin_n_node,lin_n_node))
	FF = np.zeros((lin_n_node))

	for i in range(n_elem):
	    # assemble into global stiffness
		n1 = elements[i,0]-1
		n2 = elements[i,1]-1
		n3 = elements[i,2]-1
		n4 = elements[i,3]-1
		n5 = elements[i,4]-1
		n6 = elements[i,5]-1
		n7 = elements[i,6]-1
		n8 = elements[i,7]-1
	    # N_C3D8[a,b] means a integration point and b node
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			for jmidx,jm in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
				# at each integration point
				for jsub in range(8):
					MM[im,jm] += N_C3D8[jsub,imidx]*N_C3D8[jsub,jmidx]
		
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			# at each integration point
			for jsub in range(8):
				FF[im] += N_C3D8[jsub,imidx]*GddotG_all_elem[i,jsub]

	return LA.solve(MM,FF) # at the current increment

def calc_global_burgers():

	MM = np.zeros((lin_n_node*3,lin_n_node*3))
	MMe = np.zeros((lin_n_node,lin_n_node))
	FF = np.zeros((lin_n_node*3))
	FFe1 = np.zeros((lin_n_node))
	FFe2 = np.zeros((lin_n_node))
	FFe3 = np.zeros((lin_n_node))
	for iee in range(n_elem):
	    # assemble into global stiffness
		n1 = elements[iee,0]-1
		n2 = elements[iee,1]-1
		n3 = elements[iee,2]-1
		n4 = elements[iee,3]-1
		n5 = elements[iee,4]-1
		n6 = elements[iee,5]-1
		n7 = elements[iee,6]-1
		n8 = elements[iee,7]-1
	    # N_C3D8[a,b] means a integration point and b node
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			for jmidx,jm in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
				# at each integration point
				for jsub in range(8):
					MMe[im,jm] += N_C3D8[jsub,imidx]*N_C3D8[jsub,jmidx]
		


		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			# at each integration point
			for jsub in range(8):
				FFe1[im] += N_C3D8[jsub,imidx]*burgers_all_elem[iee,jsub,0]
				FFe2[im] += N_C3D8[jsub,imidx]*burgers_all_elem[iee,jsub,1]
				FFe3[im] += N_C3D8[jsub,imidx]*burgers_all_elem[iee,jsub,2]

	for im in range(lin_n_node):
		for jm in range(lin_n_node):		
			MM[im*3:(im+1)*3,jm*3:(jm+1)*3] = MMe[im,jm]*np.eye(3,3)

	for im in range(lin_n_node):			

		FF[3*im] = FFe1[im]
		FF[3*im+1] = FFe2[im]
		FF[3*im+2] = FFe3[im]

	return LA.solve(MM,FF) # at the current increment

def calc_global_G():
	MM = np.zeros((lin_n_node*9,lin_n_node*9))
	MMe = np.zeros((lin_n_node,lin_n_node))
	FF = np.zeros((lin_n_node*9))
	FFe1 = np.zeros((lin_n_node))
	FFe2 = np.zeros((lin_n_node))
	FFe3 = np.zeros((lin_n_node))
	FFe4 = np.zeros((lin_n_node))
	FFe5 = np.zeros((lin_n_node))
	FFe6 = np.zeros((lin_n_node))
	FFe7 = np.zeros((lin_n_node))
	FFe8 = np.zeros((lin_n_node))
	FFe9 = np.zeros((lin_n_node))
	for iee in range(n_elem):
	    # assemble into global stiffness
		n1 = elements[iee,0]-1
		n2 = elements[iee,1]-1
		n3 = elements[iee,2]-1
		n4 = elements[iee,3]-1
		n5 = elements[iee,4]-1
		n6 = elements[iee,5]-1
		n7 = elements[iee,6]-1
		n8 = elements[iee,7]-1
	    # N_C3D8[a,b] means a integration point and b node
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			for jmidx,jm in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
				# at each integration point
				for jsub in range(8):
					MMe[im,jm] += N_C3D8[jsub,imidx]*N_C3D8[jsub,jmidx]
		
		for imidx,im in enumerate(np.array([n1,n2,n3,n4,n5,n6,n7,n8])):
			# at each integration point
			for jsub in range(8):
				FFe1[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,0,0]
				FFe2[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,0,1]
				FFe3[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,0,2]
				FFe4[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,1,0]
				FFe5[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,1,1]
				FFe6[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,1,2]
				FFe7[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,2,0]
				FFe8[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,2,1]
				FFe9[im] += N_C3D8[jsub,imidx]*curlFg_all_elem[iee,jsub,2,2]

	for im in range(lin_n_node):
		for jm in range(lin_n_node):		
			MM[im*9:(im+1)*9,jm*9:(jm+1)*9] = MMe[im,jm]*np.eye(9,9)

	for im in range(lin_n_node):			

		FF[9*im] = FFe1[im]
		FF[9*im+1] = FFe2[im]
		FF[9*im+2] = FFe3[im]
		FF[9*im+3] = FFe4[im]
		FF[9*im+4] = FFe5[im]
		FF[9*im+5] = FFe6[im]
		FF[9*im+6] = FFe7[im]
		FF[9*im+7] = FFe8[im]
		FF[9*im+8] = FFe9[im]

	return LA.solve(MM,FF) # at the current increment

def writeVTK(paraviewfile,step):
	paraviewfile = paraviewfile + str(step) + '.vtk'
	paraview=open(paraviewfile,'w')

	paraview.write("# vtk DataFile Version 2.0\nIncompatibility investigation increment: %f\nASCII\nDATASET UNSTRUCTURED_GRID\n" %(step))
	paraview.write("POINTS %i double\n"%lin_n_node)
	for xi in nodal_coord_xyz:
		paraview.write('%f %f %f\n' %(xi[0],xi[1],xi[2]))

	paraview.write("CELLS %i %i\n"%(n_elem,n_elem*9))
	for ei in elements:
		paraview.write('8 %i %i %i %i %i %i %i %i\n'\
                %(ei[0]-1,ei[1]-1,ei[2]-1,ei[3]-1,\
                  ei[4]-1,ei[5]-1,ei[6]-1,ei[7]-1))
	paraview.write("CELL_TYPES %i\n"%n_elem)
	for ei in elements:
		paraview.write('12\n')

	paraview.write("CELL_DATA %i\n"%(n_elem))
	paraview.write("SCALARS GddotG_G11_G12_G13_G21_G22_G23_G31_G32_G33_b1_b2_b3_magb float 14\nLOOKUP_TABLE default\n")
	for G_g_tmp,burgers_tmp,GddotG_tmp in zip(G_g_all_elem,burgers_all_elem,GddotG_all_elem):
		G_center = np.zeros((3,3))
		burgers_center = np.zeros((3,1))
		GddotG_center = 0.
		for G_g_subtmp,burgers_subtmp,GddotG_subtmp in zip(G_g_tmp,burgers_tmp,GddotG_tmp):
			G_center += G_g_subtmp
			burgers_center += burgers_subtmp
			GddotG_center += GddotG_subtmp
		G_center = G_center/8.
		burgers_center = burgers_center/8.
		GddotG_center = GddotG_center/8.

		paraview.write("%f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"\
			%(GddotG_center,G_center[0,0],G_center[0,1],G_center[0,2],\
			  G_center[1,0],G_center[1,1],G_center[1,2],\
			  G_center[2,0],G_center[2,1],G_center[2,2],\
			  burgers_center[0],burgers_center[1],burgers_center[2], LA.norm(burgers_center)))
	
	paraview.write("POINT_DATA %i\n"%lin_n_node)
	paraview.write("SCALARS GddotG_sse_theg_Ginv1_Ginv2_Ginv3_G11_G12_G13_G21_G22_G23_G31_G32_G33_s11_s22_s33 float 18\nLOOKUP_TABLE default\n")

	
	for no in range(lin_n_node):
		paraview.write("%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"\
			  %(global_GddotG[no],sse_global[no],theta_g_global[no],\
				global_G[9*no]+global_G[9*no+4]+global_G[9*no+8],\
				global_G[9*no]*global_G[9*no+4]+global_G[9*no+4]*global_G[9*no+8]+global_G[9*no+8]*global_G[9*no]-global_G[9*no+1]*global_G[9*no+3]-global_G[9*no+5]*global_G[9*no+7]-global_G[9*no+2]*global_G[9*no+6],\
				LA.det(np.reshape(global_G[9*no:9*(no+1)],(3,3))),\
				global_G[9*no], global_G[9*no+1], global_G[9*no+2],\
			  	global_G[9*no+3], global_G[9*no+4], global_G[9*no+5],\
			  	global_G[9*no+6], global_G[9*no+7], global_G[9*no+8],\
			  	s11_global[no], s22_global[no], s33_global[no]))
	
	paraview.write("VECTORS burgers float\n")
	for no in range(lin_n_node):
		paraview.write("%f %f %f\n"%(global_burgers[no*3],global_burgers[no*3+1],global_burgers[no*3+2]))
	paraview.close()

# Fg for area growth at each nodal point
def area_growth_Fg(theg_np_temp):
	
	return np.array([[np.sqrt(theg_np_temp),0.,0.],\
					 [0.,np.sqrt(theg_np_temp),0.],\
					 [0.,0.,1.]])

# Fg for volume growth at each nodal point
def volume_growth_Fg(theg_np_temp):
	
	return np.array([[theg_np_temp**(1./3.),0.,0.],\
					 [0.,theg_np_temp**(1./3.),0.],\
					 [0.,0.,theg_np_temp**(1./3.)]])

def calc_curlFg(Fg_np,Jinv,ipt,grad_xsi_N_C3D8):

	dNdx_dNdy_dNdz = np.dot(Jinv,np.reshape(grad_xsi_N_C3D8[ipt],(8,3)).T)

	temp1 = np.dot(dNdx_dNdy_dNdz[1,:],Fg_np[:,0,2])
	temp2 = np.dot(dNdx_dNdy_dNdz[2,:],Fg_np[:,0,1])
	curlFg[ipt,0,0] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[1,:],Fg_np[:,1,2])
	temp2 = np.dot(dNdx_dNdy_dNdz[2,:],Fg_np[:,1,1])
	curlFg[ipt,0,1] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[1,:],Fg_np[:,2,2])
	temp2 = np.dot(dNdx_dNdy_dNdz[2,:],Fg_np[:,2,1])
	curlFg[ipt,0,2] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[2,:],Fg_np[:,0,0])
	temp2 = np.dot(dNdx_dNdy_dNdz[0,:],Fg_np[:,0,2])
	curlFg[ipt,1,0] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[2,:],Fg_np[:,1,0])
	temp2 = np.dot(dNdx_dNdy_dNdz[0,:],Fg_np[:,1,2])
	curlFg[ipt,1,1] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[2,:],Fg_np[:,2,0])
	temp2 = np.dot(dNdx_dNdy_dNdz[0,:],Fg_np[:,2,2])
	curlFg[ipt,1,2] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[0,:],Fg_np[:,0,1])
	temp2 = np.dot(dNdx_dNdy_dNdz[1,:],Fg_np[:,0,0])
	curlFg[ipt,2,0] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[0,:],Fg_np[:,1,1])
	temp2 = np.dot(dNdx_dNdy_dNdz[1,:],Fg_np[:,1,0])
	curlFg[ipt,2,1] = temp1 - temp2

	temp1 = np.dot(dNdx_dNdy_dNdz[0,:],Fg_np[:,2,1])
	temp2 = np.dot(dNdx_dNdy_dNdz[1,:],Fg_np[:,2,0])
	curlFg[ipt,2,2] = temp1 - temp2
	return curlFg[ipt]

# import mesh information
meshfile = open(inputfile,'r').readlines()
n_node = nodes
node_X = np.zeros((n_node,3))
for i in range(n_node):
	aux = meshfile[16+i].split(',')
	node_X[i,0] = float(aux[1])
	node_X[i,1] = float(aux[2])
	node_X[i,2] = float(aux[3])
n_elem = elements
elements = np.zeros((n_elem,8),dtype=int)
for i in range(n_elem):
	aux = meshfile[17+n_node+i].split(',')
	elements[i,0] = int(aux[1])
	elements[i,1] = int(aux[2])
	elements[i,2] = int(aux[3])
	elements[i,3] = int(aux[4])
	elements[i,4] = int(aux[5])
	elements[i,5] = int(aux[6])
	elements[i,6] = int(aux[7])
	elements[i,7] = int(aux[8])

node_x[:,0] = node_X[:,0]
node_y[:,0] = node_X[:,1]
node_z[:,0] = node_X[:,2]

# integration points are followed to the abaqus convention which is different with the common ordering:
#common: 1,2,3,4,5,6,7,8 that follow the ordering of actual nodes
#abaqus: 1,2,4,3,5,6,8,7
#But I use the common ordering and import abaqus data after changing their ordering to the commone one
g_p_C3D8 = np.array([[-1/np.sqrt(3.),-1/np.sqrt(3.),-1/np.sqrt(3.)],\
                  [ 1/np.sqrt(3.),-1/np.sqrt(3.),-1/np.sqrt(3.)],\
                  [ 1/np.sqrt(3.), 1/np.sqrt(3.),-1/np.sqrt(3.)],\
                  [-1/np.sqrt(3.), 1/np.sqrt(3.),-1/np.sqrt(3.)],\
                  [-1/np.sqrt(3.),-1/np.sqrt(3.), 1/np.sqrt(3.)],\
                  [ 1/np.sqrt(3.),-1/np.sqrt(3.), 1/np.sqrt(3.)],\
                  [ 1/np.sqrt(3.), 1/np.sqrt(3.), 1/np.sqrt(3.)],\
                  [-1/np.sqrt(3.), 1/np.sqrt(3.), 1/np.sqrt(3.)]])
w_g_C3D8 = np.ones((8))
# shape functions at all integration points and all nodes
N_C3D8 = np.zeros((8,8))
for i in range(8):
	N_C3D8[i,0] = (1-g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8
	N_C3D8[i,1] = (1+g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8
	N_C3D8[i,2] = (1+g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8
	N_C3D8[i,3] = (1-g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8
	N_C3D8[i,4] = (1-g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8
	N_C3D8[i,5] = (1+g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8
	N_C3D8[i,6] = (1+g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8
	N_C3D8[i,7] = (1-g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8

# gradient of shape functions wrt xsi
# gradient of shape functions wrt xsi
grad_xsi_N_C3D8 = np.zeros((8,8,3,1)) # (ith ipt, no. of integration points, (3x1))
# eg) grad_xsi_N_C3D8[0,1,2,0] # isopara value at 1st int point, inserting it to 2nd int point, derivative wrt 3rd isopara
for i in range(8):
	grad_xsi_N_C3D8[i,0] = [[-1*(1-g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8],\
					   [-1*(1-g_p_C3D8[i,0])*(1-g_p_C3D8[i,2])/8],\
					   [-1*(1-g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])/8]]
	grad_xsi_N_C3D8[i,1] = [[+1*(1-g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8],\
					   [-1*(1+g_p_C3D8[i,0])*(1-g_p_C3D8[i,2])/8],\
					   [-1*(1+g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])/8]]
	grad_xsi_N_C3D8[i,2] = [[+1*(1+g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8],\
					   [+1*(1+g_p_C3D8[i,0])*(1-g_p_C3D8[i,2])/8],\
					   [-1*(1+g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])/8]]
	grad_xsi_N_C3D8[i,3] = [[-1*(1+g_p_C3D8[i,1])*(1-g_p_C3D8[i,2])/8],\
					   [+1*(1-g_p_C3D8[i,0])*(1-g_p_C3D8[i,2])/8],\
					   [-1*(1-g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])/8]]
	grad_xsi_N_C3D8[i,4] = [[-1*(1-g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8],\
					   [-1*(1-g_p_C3D8[i,0])*(1+g_p_C3D8[i,2])/8],\
					   [+1*(1-g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])/8]]
	grad_xsi_N_C3D8[i,5] = [[+1*(1-g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8],\
					   [-1*(1+g_p_C3D8[i,0])*(1+g_p_C3D8[i,2])/8],\
					   [+1*(1+g_p_C3D8[i,0])*(1-g_p_C3D8[i,1])/8]]
	grad_xsi_N_C3D8[i,6] = [[+1*(1+g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8],\
					   [+1*(1+g_p_C3D8[i,0])*(1+g_p_C3D8[i,2])/8],\
					   [+1*(1+g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])/8]]
	grad_xsi_N_C3D8[i,7] = [[-1*(1+g_p_C3D8[i,1])*(1+g_p_C3D8[i,2])/8],\
					   [+1*(1-g_p_C3D8[i,0])*(1+g_p_C3D8[i,2])/8],\
					   [+1*(1-g_p_C3D8[i,0])*(1+g_p_C3D8[i,1])/8]]

## Loop over elements 
for increment in range(increments):

	theta_g_global = calc_global_lin(theg,lin_n_node)
	sse_global = calc_global_lin(sse,lin_n_node)
	s11_global = calc_global_lin(s11,lin_n_node)
	s22_global = calc_global_lin(s22,lin_n_node)
	s33_global = calc_global_lin(s33,lin_n_node)
	G_g_all_elem = np.zeros((n_elem,8,3,3))
	GddotG_all_elem = np.zeros((n_elem,8,1))
	slip_stress_all_elem = np.zeros((n_elem,8,1))
	slip_stress_all_elem2 = np.zeros((n_elem,8,1))
	curlFg_all_elem = np.zeros((n_elem,8,3,3))
	burgers_all_elem = np.zeros((n_elem,8,3,1))
	stress_all_elem = np.zeros((n_elem,8,3,1))
	nodal_coord_xyz = np.zeros((lin_n_node,3))

	for i in range(n_elem):
		n1 = elements[i,0]-1
		n2 = elements[i,1]-1
		n3 = elements[i,2]-1
		n4 = elements[i,3]-1
		n5 = elements[i,4]-1
		n6 = elements[i,5]-1
		n7 = elements[i,6]-1
		n8 = elements[i,7]-1

		# theta_g at each nodal point
		theg_np1 = theta_g_global[n1]
		theg_np2 = theta_g_global[n2]
		theg_np3 = theta_g_global[n3]
		theg_np4 = theta_g_global[n4]
		theg_np5 = theta_g_global[n5]
		theg_np6 = theta_g_global[n6]
		theg_np7 = theta_g_global[n7]
		theg_np8 = theta_g_global[n8]

		# Fg for area growth at each nodal point

		Fg_np1 = area_growth_Fg(theg_np1)
		Fg_np2 = area_growth_Fg(theg_np2)
		Fg_np3 = area_growth_Fg(theg_np3)
		Fg_np4 = area_growth_Fg(theg_np4)
		Fg_np5 = area_growth_Fg(theg_np5)
		Fg_np6 = area_growth_Fg(theg_np6)
		Fg_np7 = area_growth_Fg(theg_np7)
		Fg_np8 = area_growth_Fg(theg_np8)

		Fg_np = np.zeros((8,3,3))
		Fg_np[0] = Fg_np1
		Fg_np[1] = Fg_np2
		Fg_np[2] = Fg_np3
		Fg_np[3] = Fg_np4
		Fg_np[4] = Fg_np5
		Fg_np[5] = Fg_np6
		Fg_np[6] = Fg_np7
		Fg_np[7] = Fg_np8

		# theta_g at each integration point
		theg_ip1 = theg[i,0,increment]
		theg_ip2 = theg[i,1,increment]
		theg_ip3 = theg[i,3,increment]
		theg_ip4 = theg[i,2,increment]
		theg_ip5 = theg[i,4,increment]
		theg_ip6 = theg[i,5,increment]
		theg_ip7 = theg[i,7,increment]
		theg_ip8 = theg[i,6,increment]
		# Fg for area growth at each integration point
		Fg_ip1 = area_growth_Fg(theg_ip1)
		Fg_ip2 = area_growth_Fg(theg_ip2)
		Fg_ip3 = area_growth_Fg(theg_ip3)
		Fg_ip4 = area_growth_Fg(theg_ip4)
		Fg_ip5 = area_growth_Fg(theg_ip5)
		Fg_ip6 = area_growth_Fg(theg_ip6)
		Fg_ip7 = area_growth_Fg(theg_ip7)
		Fg_ip8 = area_growth_Fg(theg_ip8)

		Fg_ip = np.zeros((8,3,3))
		Fg_ip[0] = Fg_ip1
		Fg_ip[1] = Fg_ip2
		Fg_ip[2] = Fg_ip3
		Fg_ip[3] = Fg_ip4
		Fg_ip[4] = Fg_ip5
		Fg_ip[5] = Fg_ip6
		Fg_ip[6] = Fg_ip7
		Fg_ip[7] = Fg_ip8

		# normal vector at each integration point
		normal_xyz = np.zeros((8,3,1))
		# tt is integration point
		
		normal_xyz[0] = np.array([[normal_x[i,0,increment]],\
								   [normal_y[i,0,increment]],\
								   [normal_z[i,0,increment]]])
		normal_xyz[1] = np.array([[normal_x[i,1,increment]],\
								   [normal_y[i,1,increment]],\
								   [normal_z[i,1,increment]]])
		normal_xyz[2] = np.array([[normal_x[i,3,increment]],\
								   [normal_y[i,3,increment]],\
								   [normal_z[i,3,increment]]])
		normal_xyz[3] = np.array([[normal_x[i,2,increment]],\
								   [normal_y[i,2,increment]],\
								   [normal_z[i,2,increment]]])
		normal_xyz[4] = np.array([[normal_x[i,4,increment]],\
								   [normal_y[i,4,increment]],\
								   [normal_z[i,4,increment]]])
		normal_xyz[5] = np.array([[normal_x[i,5,increment]],\
								   [normal_y[i,5,increment]],\
								   [normal_z[i,5,increment]]])
		normal_xyz[6] = np.array([[normal_x[i,7,increment]],\
								   [normal_y[i,7,increment]],\
								   [normal_z[i,7,increment]]])
		normal_xyz[7] = np.array([[normal_x[i,6,increment]],\
								   [normal_y[i,6,increment]],\
								   [normal_z[i,6,increment]]])

		normal_x1 = normal_x[i,0,increment]
		normal_y1 = normal_y[i,0,increment]
		normal_z1 = normal_z[i,0,increment]
		normal_x2 = normal_x[i,1,increment]
		normal_y2 = normal_y[i,1,increment]
		normal_z2 = normal_z[i,1,increment]
		normal_x3 = normal_x[i,3,increment]
		normal_y3 = normal_y[i,3,increment]
		normal_z3 = normal_z[i,3,increment]
		normal_x4 = normal_x[i,2,increment]
		normal_y4 = normal_y[i,2,increment]
		normal_z4 = normal_z[i,2,increment]
		normal_x5 = normal_x[i,4,increment]
		normal_y5 = normal_y[i,4,increment]
		normal_z5 = normal_z[i,4,increment]
		normal_x6 = normal_x[i,5,increment]
		normal_y6 = normal_y[i,5,increment]
		normal_z6 = normal_z[i,5,increment]
		normal_x7 = normal_x[i,7,increment]
		normal_y7 = normal_y[i,7,increment]
		normal_z7 = normal_z[i,7,increment]
		normal_x8 = normal_x[i,6,increment]
		normal_y8 = normal_y[i,6,increment]
		normal_z8 = normal_z[i,6,increment]		
		# node numbers at this element
		node1 = elements[i,0]
		node2 = elements[i,1]
		node3 = elements[i,2]
		node4 = elements[i,3]
		node5 = elements[i,4]
		node6 = elements[i,5]
		node7 = elements[i,6]
		node8 = elements[i,7]

		# nodal coordinate at this element in reference configuration
		x1 = node_x[node1-1,0]
		y1 = node_y[node1-1,0]
		z1 = node_z[node1-1,0]
		x2 = node_x[node2-1,0]
		y2 = node_y[node2-1,0]
		z2 = node_z[node2-1,0]
		x3 = node_x[node3-1,0]
		y3 = node_y[node3-1,0]
		z3 = node_z[node3-1,0]
		x4 = node_x[node4-1,0]
		y4 = node_y[node4-1,0]
		z4 = node_z[node4-1,0]
		x5 = node_x[node5-1,0]
		y5 = node_y[node5-1,0]
		z5 = node_z[node5-1,0]
		x6 = node_x[node6-1,0]
		y6 = node_y[node6-1,0]
		z6 = node_z[node6-1,0]
		x7 = node_x[node7-1,0]
		y7 = node_y[node7-1,0]
		z7 = node_z[node7-1,0]
		x8 = node_x[node8-1,0]
		y8 = node_y[node8-1,0]
		z8 = node_z[node8-1,0]

		nodal_coord_xyz[node1-1] = np.array([node_x[node1-1,increment],node_y[node1-1,increment],node_z[node1-1,increment]])
		nodal_coord_xyz[node2-1] = np.array([node_x[node2-1,increment],node_y[node2-1,increment],node_z[node2-1,increment]])
		nodal_coord_xyz[node3-1] = np.array([node_x[node3-1,increment],node_y[node3-1,increment],node_z[node3-1,increment]])
		nodal_coord_xyz[node4-1] = np.array([node_x[node4-1,increment],node_y[node4-1,increment],node_z[node4-1,increment]])
		nodal_coord_xyz[node5-1] = np.array([node_x[node5-1,increment],node_y[node5-1,increment],node_z[node5-1,increment]])
		nodal_coord_xyz[node6-1] = np.array([node_x[node6-1,increment],node_y[node6-1,increment],node_z[node6-1,increment]])
		nodal_coord_xyz[node7-1] = np.array([node_x[node7-1,increment],node_y[node7-1,increment],node_z[node7-1,increment]])
		nodal_coord_xyz[node8-1] = np.array([node_x[node8-1,increment],node_y[node8-1,increment],node_z[node8-1,increment]])

		curlFg = np.zeros((8,3,3))
		G_g = np.zeros((8,3,3))
		GddotG = np.zeros((8,1))
		slip_stress = np.zeros((8,1))
		slip_stress2 = np.zeros((8,1))
		burgers = np.zeros((8,3,1))
		stress = np.zeros((8,3,1))
		J = np.zeros((3,3))

		for ipt in range(8):
			J[0,0] = grad_xsi_N_C3D8[ipt,0,0,0]*x1 +\
					 grad_xsi_N_C3D8[ipt,1,0,0]*x2 +\
	    			 grad_xsi_N_C3D8[ipt,2,0,0]*x3 +\
	    			 grad_xsi_N_C3D8[ipt,3,0,0]*x4 +\
	    			 grad_xsi_N_C3D8[ipt,4,0,0]*x5 +\
	    			 grad_xsi_N_C3D8[ipt,5,0,0]*x6 +\
	    			 grad_xsi_N_C3D8[ipt,6,0,0]*x7 +\
	    			 grad_xsi_N_C3D8[ipt,7,0,0]*x8

			J[0,1] = grad_xsi_N_C3D8[ipt,0,0,0]*y1 +\
	    			 grad_xsi_N_C3D8[ipt,1,0,0]*y2 +\
	    			 grad_xsi_N_C3D8[ipt,2,0,0]*y3 +\
	    			 grad_xsi_N_C3D8[ipt,3,0,0]*y4 +\
	    			 grad_xsi_N_C3D8[ipt,4,0,0]*y5 +\
	    			 grad_xsi_N_C3D8[ipt,5,0,0]*y6 +\
	    			 grad_xsi_N_C3D8[ipt,6,0,0]*y7 +\
	    			 grad_xsi_N_C3D8[ipt,7,0,0]*y8

			J[0,2] = grad_xsi_N_C3D8[ipt,0,0,0]*z1 +\
	    			 grad_xsi_N_C3D8[ipt,1,0,0]*z2 +\
	    			 grad_xsi_N_C3D8[ipt,2,0,0]*z3 +\
	    			 grad_xsi_N_C3D8[ipt,3,0,0]*z4 +\
	    			 grad_xsi_N_C3D8[ipt,4,0,0]*z5 +\
	    			 grad_xsi_N_C3D8[ipt,5,0,0]*z6 +\
	    			 grad_xsi_N_C3D8[ipt,6,0,0]*z7 +\
	    			 grad_xsi_N_C3D8[ipt,7,0,0]*z8

			J[1,0] = grad_xsi_N_C3D8[ipt,0,1,0]*x1 +\
	    			 grad_xsi_N_C3D8[ipt,1,1,0]*x2 +\
	    			 grad_xsi_N_C3D8[ipt,2,1,0]*x3 +\
	    			 grad_xsi_N_C3D8[ipt,3,1,0]*x4 +\
	    			 grad_xsi_N_C3D8[ipt,4,1,0]*x5 +\
	    			 grad_xsi_N_C3D8[ipt,5,1,0]*x6 +\
	    			 grad_xsi_N_C3D8[ipt,6,1,0]*x7 +\
	    			 grad_xsi_N_C3D8[ipt,7,1,0]*x8

			J[1,1] = grad_xsi_N_C3D8[ipt,0,1,0]*y1 +\
	    			 grad_xsi_N_C3D8[ipt,1,1,0]*y2 +\
	    			 grad_xsi_N_C3D8[ipt,2,1,0]*y3 +\
	    			 grad_xsi_N_C3D8[ipt,3,1,0]*y4 +\
	    			 grad_xsi_N_C3D8[ipt,4,1,0]*y5 +\
	    			 grad_xsi_N_C3D8[ipt,5,1,0]*y6 +\
	    			 grad_xsi_N_C3D8[ipt,6,1,0]*y7 +\
	    			 grad_xsi_N_C3D8[ipt,7,1,0]*y8

			J[1,2] = grad_xsi_N_C3D8[ipt,0,1,0]*z1 +\
	    			 grad_xsi_N_C3D8[ipt,1,1,0]*z2 +\
	    			 grad_xsi_N_C3D8[ipt,2,1,0]*z3 +\
	    			 grad_xsi_N_C3D8[ipt,3,1,0]*z4 +\
	    			 grad_xsi_N_C3D8[ipt,4,1,0]*z5 +\
	    			 grad_xsi_N_C3D8[ipt,5,1,0]*z6 +\
	    			 grad_xsi_N_C3D8[ipt,6,1,0]*z7 +\
	    			 grad_xsi_N_C3D8[ipt,7,1,0]*z8

			J[2,0] = grad_xsi_N_C3D8[ipt,0,2,0]*x1 +\
	    			 grad_xsi_N_C3D8[ipt,1,2,0]*x2 +\
	    			 grad_xsi_N_C3D8[ipt,2,2,0]*x3 +\
	    			 grad_xsi_N_C3D8[ipt,3,2,0]*x4 +\
	    			 grad_xsi_N_C3D8[ipt,4,2,0]*x5 +\
	    			 grad_xsi_N_C3D8[ipt,5,2,0]*x6 +\
	    			 grad_xsi_N_C3D8[ipt,6,2,0]*x7 +\
	    			 grad_xsi_N_C3D8[ipt,7,2,0]*x8

			J[2,1] = grad_xsi_N_C3D8[ipt,0,2,0]*y1 +\
	    			 grad_xsi_N_C3D8[ipt,1,2,0]*y2 +\
	    			 grad_xsi_N_C3D8[ipt,2,2,0]*y3 +\
	    			 grad_xsi_N_C3D8[ipt,3,2,0]*y4 +\
	    			 grad_xsi_N_C3D8[ipt,4,2,0]*y5 +\
	    			 grad_xsi_N_C3D8[ipt,5,2,0]*y6 +\
	    			 grad_xsi_N_C3D8[ipt,6,2,0]*y7 +\
	    			 grad_xsi_N_C3D8[ipt,7,2,0]*y8

			J[2,2] = grad_xsi_N_C3D8[ipt,0,2,0]*z1 +\
	    			 grad_xsi_N_C3D8[ipt,1,2,0]*z2 +\
	    			 grad_xsi_N_C3D8[ipt,2,2,0]*z3 +\
	    			 grad_xsi_N_C3D8[ipt,3,2,0]*z4 +\
	    			 grad_xsi_N_C3D8[ipt,4,2,0]*z5 +\
	    			 grad_xsi_N_C3D8[ipt,5,2,0]*z6 +\
	    			 grad_xsi_N_C3D8[ipt,6,2,0]*z7 +\
	    			 grad_xsi_N_C3D8[ipt,7,2,0]*z8
	    	# inverse of Jacobian
			Jinv = LA.inv(J)
	    	# Gradient of shape function wrt global coordinates
	    	# 3x8 matrix
			curlFg[ipt] = calc_curlFg(Fg_np,Jinv,ipt,grad_xsi_N_C3D8)
		
			if LA.det(Fg_ip[ipt]) == 0.:
				G_g[ipt] = np.dot(Fg_ip[ipt],curlFg[ipt])
				print('at increment: %s, Fg_ip is singular'%increment)
			else:
				G_g[ipt] = 1./LA.det(Fg_ip[ipt])*np.dot(Fg_ip[ipt],curlFg[ipt])
			
			GddotG[ipt] = np.tensordot(G_g[ipt],G_g[ipt])
			burgers[ipt] = np.dot(curlFg[ipt].T,normal_xyz[ipt]/LA.norm(normal_xyz[ipt]))
			#burgers[ipt] = np.dot(G_g[ipt].T,normal_xyz[ipt])
			#slip_stress[ipt] = np.dot(burgers[ipt].T,Senbar_ip[ipt])
			#slip_stress2[ipt] = np.dot(np.dot(burgers[ipt].T,pk2_ip[ipt]),burgers[ipt])

		G_g_all_elem[i] = G_g
		GddotG_all_elem[i] = GddotG
		burgers_all_elem[i] = burgers
		curlFg_all_elem[i] = curlFg
		#slip_stress_all_elem[i] = slip_stress
		#slip_stress_all_elem2[i] = slip_stress2	
	

	# calculate burgers vector at each node
	global_burgers = calc_global_burgers()
	global_G = calc_global_G()
	global_GddotG = calc_global_GddotG()
	#global_slip_stress = calc_global_slipstress()
	#global_slip_stress2 = calc_global_slipstress2()
	# make vtk file
	paraviewfile = 'biaxial_area_radial_fiber'
	step = increment + 1
	writeVTK(paraviewfile,step)
