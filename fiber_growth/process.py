import numpy as np
import os
import shutil

nu = 0.4
# mu, n, k, m
ParamVal = np.array([0.55, 0.819, 0.419, 0.035])

parfile = open('param.param','w')
parfile.write('*PARAMETER\n')
parfile.write('lam= ')
parfile.write('%.3f\n'%(ParamVal[0]*((2.+2.*nu)/(3.-6.*nu)-2./3.)))
parfile.write('mu= ')
parfile.write('%.3f\n'% ParamVal[0])
parfile.write('xn0_1=0.\n')
parfile.write('xn0_2=1.\n')
parfile.write('xn0_3=0.\n')
parfile.write('kk= ')
parfile.write('%.3f\n'% ParamVal[2])	
parfile.write('tcrt=1.1\n')
parfile.write('mm= ')
parfile.write('%.3f\n'% ParamVal[3])	
parfile.write('nn= ')
parfile.write('%.3f\n'% ParamVal[1])	
parfile.write('t_init1=0.01\n')
parfile.write('t_max1=0.01\n')
parfile.close()

os.system("abaqus job=JobName input=InputFile_quad_pillar user=umat_incompatibility_fiber ask_delete=OFF cpus=12 interactive")
os.system("abaqus viewer noGUI=postprocessing.py")

