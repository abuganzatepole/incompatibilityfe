c...  ------------------------------------------------------------------
      subroutine sdvini(statev,coords,nstatv,ncrds,noel,npt,layer,kspt)
c...  ------------------------------------------------------------------
      include 'aba_param.inc'

      dimension statev(nstatv)

      statev(1)=1.0d0    ! the_g
      statev(2)=1.0d0    ! the_e
      statev(3)=1.0d0    ! the
      statev(4)=1.0d0    ! x of deformed normal vector
      statev(5)=1.0d0    ! y of deformed normal vector
      statev(6)=1.0d0    ! z of deformed normal vector
      statev(7)=1.0d0    ! x_coordinate in reference
      statev(8)=1.0d0    ! y_coordinate in reference
      statev(9)=1.0d0    ! z_coordinate in reference
      statev(10)=1.0d0    ! strain energy
      statev(11)=1.0d0    ! x of undeformed fiber vector
      statev(12)=1.0d0    ! y of undeformed fiber vector
      statev(13)=1.0d0    ! z of undeformed fiber vector
      statev(14)=1.0d0    ! x of deformed fiber vector
      statev(15)=1.0d0    ! y of deformed fiber vector
      statev(16)=1.0d0    ! z of deformed fiber vector
      statev(17) = 1.0d0
      statev(18) = 1.0d0
      statev(19) = 1.0d0
      statev(20) = 1.0d0
      statev(21) = 1.0d0
      statev(22) = 1.0d0
      statev(23) = 1.0d0
      statev(24) = 1.0d0
      statev(25) = 1.0d0
      statev(26) = 1.0d0
      return
      end



c...  ------------------------------------------------------------------
c     user amplitude subroutine
      Subroutine UAMP(
     *     ampName, time, ampValueOld, dt, nProps, props, nSvars, svars,
     *     lFlagsInfo, nSensor, sensorValues, sensorNames,
     *     jSensorLookUpTable,
C          to be defined
     *     ampValueNew,
     *     lFlagsDefine,
     *     AmpDerivative, AmpSecDerivative, AmpIncIntegral,
     *     AmpIncDoubleIntegral)

      include 'aba_param.inc'

C     svars - additional state variables, similar to (V)UEL

C     time indices
      parameter (iStepTime        = 1,
     *           iTotalTime       = 2,
     *           nTime            = 2)
C     flags passed in for information
      parameter (iInitialization   = 1,
     *           iRegularInc       = 2,
     *           nFlagsInfo        = 2)
C     optional flags to be defined
      parameter (iComputeDeriv       = 1,
     *           iComputeSecDeriv    = 2,
     *           iComputeInteg       = 3,
     *           iComputeDoubleInteg = 4,
     *           iStopAnalysis       = 5,
     *           iConcludeStep       = 6,
     *           nFlagsDefine        = 6)

      dimension time(nTime), lFlagsInfo(nFlagsInfo),
     *          lFlagsDefine(nFlagsDefine)
      dimension jSensorLookUpTable(*)
      common /pressure/ porepres
      common /porepres_pre/ porepres_pre
      common /volume/ porevol
      common /preKiterm/ preKiterm
      common /preerr/ preerr
      common /tStart_before/ tStart_before

      double precision porepres,porevol,preKiterm,
     #                 preerr,tStart_before,porepres_pre
      dimension sensorValues(nSensor), svars(nSvars), props(nProps)

      character*80 sensorNames(nSensor)
      character*80 ampName

c     get sensor values first
      porevol = GetSensorValue('VOL_EXP',jSensorLookUpTable,
     *                           sensorValues)
      porepres_pre = GetSensorValue('PRES_EXP',jSensorLookUpTable,
     *                           sensorValues)
c      ampValueOld = 0.008
      ampValueOld = 0.0001


      if (ampName(1:7) .eq. 'AMP_PID' ) then

C        User code to compute  ampValue = F(sensors)
         if (lFlagsInfo(iInitialization).eq.1) then
c	    tim = time(iStepTime)
            tStart_before = time(iStepTime)
            ampValueNew  = ampValueOld
            porepres = ampValueOld

         else
c           Example: f(t) = t

            tim = time(iStepTime)
            tStart = tim - dt
            tEnd   = tim
            call pid_control(dt,tim,tStart)
            ampValueNew = porepres


C           stop the analysis, if desired
            if (rfAve.gt.1000000) lFlagsDefine(iStopAnalysis)=1
         end if

      end if

      return
      end

c...  ------------------------------------------------------------------
      subroutine pid_control(dt,tim,tStart)
      implicit none
      common /pressure/ porepres
      common /porepres_pre/ porepres_pre
      common /volume/ porevol
      common /preKiterm/ preKiterm
      common /preerr/ preerr
      common /tStart_before/ tStart_before


      real*8  tim,tStart, tStart_before
      real*8  prevol, curvol, err, SP, Kp, Ki, Kd, dt
      real*8  output,porepres,porevol,preKiterm,preerr,porepres_pre


      curvol = porevol


      SP = 69000.d0

      Kp = 1.0d0
      Ki = 0.5d0
      Kd = 0.01d0


      err = SP - curvol

      if (tStart_before.eq.tStart) then
        porevol = porevol
        tStart_before = tStart
      else

        output = Kp*err + preKiterm + Ki*dt + Kd*(err-preerr)/dt


        if (tim.gt.0.02d0) then
          porepres= porepres_pre + 10.d0*output/60000000.d0 !new pres for 30cc
c          porepres= porepres_pre + 7.d0*output/100000000.d0	
        else
          porepres= porepres_pre + output/70000000.d0 !new pres for 30cc

        end if
ccccccccccccccccc
ccccccccccccccccc
        porevol = curvol + output ! new volume
        preKiterm = preKiterm + Ki*dt ! new Ki term
        preerr = err ! new error term


        tStart_before = tStart

      end if


      RETURN
      END
c...  ------------------------------------------------------------------


c...  ------------------------------------------------------------------
      subroutine umat(stress,statev,ddsdde,sse,spd,scd,
     #rpl,ddsddt,drplde,drpldt,
     #stran,dstran,time,dtime,temp,dtemp,predef,dpred,cmname,
     #ndi,nshr,ntens,nstatv,props,nprops,coords,drot,pnewdt,
     #celent,dfgrd0,dfgrd1,noel,npt,layer,kspt,kstep,kinc)
c...  ------------------------------------------------------------------
      include 'aba_param.inc'

      character*80 cmname
      dimension stress(ntens),statev(nstatv),
     #ddsdde(ntens,ntens),ddsddt(ntens),drplde(ntens),
     #stran(ntens),dstran(ntens),time(2),predef(1),dpred(1),
     #props(nprops),coords(3),drot(3,3),dfgrd0(3,3),dfgrd1(3,3)

      call umat_area_stretch(stress,statev,ddsdde,sse,
     #                       time,dtime,coords,props,dfgrd1,
     #                       ntens,ndi,nshr,nstatv,nprops,
     #                       noel,npt,kstep,kinc)

      return
      end

c...  ------------------------------------------------------------------
      subroutine umat_area_stretch(stress,statev,ddsdde,sse,
     #                             time,dtime,coords,props,dfgrd1,
     #                             ntens,ndi,nshr,nstatv,nprops,
     #                             noel,npt,kstep,kinc)
c...  ------------------------------------------------------------------
c * " This UMAT is written for a Neohookean constitutive material.
c * " It implements area-stretch-driven area growth in the plane normal to xn0.
c * " It is not suitable for use with local material directions.

c * " Written by Maria Holland in Aug 2012
c * " Updated by Maria Holland in Sep 2017
c * " Edited by Taeksang Lee in Mar 2019
c...  ------------------------------------------------------------------

      implicit none

c...  variables to be defined
      real*8  stress(ntens), ddsdde(ntens,ntens), statev(nstatv), sse

c...  variables passed in for information
      real*8  time(2), dtime, coords(3), props(nprops), dfgrd1(3,3)
      integer ntens, ndi, nshr, nstatv, nprops, noel, npt, kstep, kinc

c...  material properties
      real*8  lam, mu, f0(3), hill2, tcr, hill3, hill1,xn0(3)

c...  local variables
      integer i, j, nitl
      real*8  norm, f(3), finv(3,3), ftn0(3), norm_nn, fginv(3,3)
      real*8  the, theg, theg_n, detf, theta, R, pi
      real*8  fe(3,3), fg(3,3), be(6), b(6), detfe, lnJe
      real*8  phig, res, dres, dot_theg_dtheg, dot_theg, stress_temp(6)
      real*8  fac, cg_ij(6), cg_kl(6), Amat(3,3), Bmat(3,3), stressmat(3,3)
      real*8  xi(6), xtol, mat1(3,3), mat2(3,3)
      real*8 axis_y(3), hoop_g(3), hoop_g_temp(3),coords_g(3)
      real*8 pk2e(6), pk2emat(3,3), feinv(3,3), xn_tmp(3),xn0xn_tmp(3,3),pk2etmp(3,3)
      real*8 radial_g(3), axial_g(3), hoop(3), hoop_temp(3),radial(3)

      data xi/1.d0,1.d0,1.d0,0.d0,0.d0,0.d0/
      xtol = 1.d-12
      pi = 3.1415927

c...  initialize material parameters
      lam   = props(1)    ! lame constant
      mu    = props(2)    ! shear modulus
      f0(1) = props(3)   ! f0[1] fiber orientation
      f0(2) = props(4)   ! f0[2] fiber orientation
      f0(3) = props(5)   ! f0[3] fiber orientation
      hill2 = props(6)    ! growth rate para1
      tcr   = props(7)    ! critical stretch
      hill3 = props(8)        ! growth rate para2
      hill1   = props(9)    ! exponential growth rate
      xn0(1) = props(10)   ! normal vector
      xn0(2) = props(11)   ! normal vector
      xn0(3) = props(12)   ! normal vector

c...  save coordinate at integration point in reference configuration

      if (kinc.eq.1) then
        statev(7) = coords(1)
        statev(8) = coords(2)
        statev(9) = coords(3)

      	statev(11) = f0(1)
     	statev(12) = f0(2)
      	statev(13) = f0(3)
      end if

c...  unit vector of fiber orientation
      f(1) = (dfgrd1(1,1)*f0(1) + dfgrd1(1,2)*f0(2) + dfgrd1(1,3)*f0(3))
      f(2) = (dfgrd1(2,1)*f0(1) + dfgrd1(2,2)*f0(2) + dfgrd1(2,3)*f0(3))
      f(3) = (dfgrd1(3,1)*f0(1) + dfgrd1(3,2)*f0(2) + dfgrd1(3,3)*f0(3))

      statev(14) = dfgrd1(2,1)*f0(1)
      statev(15) = dfgrd1(2,2)*f0(2)
      statev(16) = dfgrd1(2,3)*f0(3)


      statev(17) = dfgrd1(1,1)
      statev(18) = dfgrd1(1,2)
      statev(19) = dfgrd1(1,3)
      statev(20) = dfgrd1(2,1)
      statev(21) = dfgrd1(2,2)
      statev(22) = dfgrd1(2,3)
      statev(23) = dfgrd1(3,1)
      statev(24) = dfgrd1(3,2)
      statev(25) = dfgrd1(3,3)

      the = (f(1)*f(1) + f(2)*f(2) + f(3)*f(3))**0.5d0

c...  calculate determinant of deformation gradient
      detf = +dfgrd1(1,1)*(dfgrd1(2,2)*dfgrd1(3,3)-dfgrd1(2,3)*dfgrd1(3,2))
     #       -dfgrd1(1,2)*(dfgrd1(2,1)*dfgrd1(3,3)-dfgrd1(2,3)*dfgrd1(3,1))
     #       +dfgrd1(1,3)*(dfgrd1(2,1)*dfgrd1(3,2)-dfgrd1(2,2)*dfgrd1(3,1))

c...  calculate area stretch
      finv(1,1) = (+dfgrd1(2,2)*dfgrd1(3,3) - dfgrd1(2,3)*dfgrd1(3,2))/detf
      finv(1,2) = (-dfgrd1(1,2)*dfgrd1(3,3) + dfgrd1(1,3)*dfgrd1(3,2))/detf
      finv(1,3) = (+dfgrd1(1,2)*dfgrd1(2,3) - dfgrd1(1,3)*dfgrd1(2,2))/detf
      finv(2,1) = (-dfgrd1(2,1)*dfgrd1(3,3) + dfgrd1(2,3)*dfgrd1(3,1))/detf
      finv(2,2) = (+dfgrd1(1,1)*dfgrd1(3,3) - dfgrd1(1,3)*dfgrd1(3,1))/detf
      finv(2,3) = (-dfgrd1(1,1)*dfgrd1(2,3) + dfgrd1(1,3)*dfgrd1(2,1))/detf
      finv(3,1) = (+dfgrd1(2,1)*dfgrd1(3,2) - dfgrd1(2,2)*dfgrd1(3,1))/detf
      finv(3,2) = (-dfgrd1(1,1)*dfgrd1(3,2) + dfgrd1(1,2)*dfgrd1(3,1))/detf
      finv(3,3) = (+dfgrd1(1,1)*dfgrd1(2,2) - dfgrd1(1,2)*dfgrd1(2,1))/detf



c...  obtain state variable history
      theg_n = statev(1)
      theg   = theg_n

c...  update state variables

c      if (kinc > 2) then
c        theg = theg + dtime/1.d0*1.d0/8.d0*(statev(7) + statev(8))
c        theg = theg + dtime/1.d0*1.d0/4.d0/(1.d0+sqrt(3.d0))*sqrt(3.d0)*(statev(7)/sqrt(3.d0) + statev(8))
c      end if

c      if (kinc > 2) then
      theg = theg + dtime/1.d0*1.d0/4.d0*statev(8)
c      end if

      statev(1) = theg
      statev(2) = the/theg
      statev(3) = the
      

c...  fiber growth
c...  calculate growth tensor Fg = I + (theg - 1) f0 \otimes f0
c...  f = (cos,sin,0)

      fg(1,1) = 1.d0 + (theg - 1.d0)*f0(1)*f0(1)
      fg(1,2) = 0.d0 + (theg - 1.d0)*f0(1)*f0(2)
      fg(1,3) = 0.d0 + (theg - 1.d0)*f0(1)*f0(3)
      fg(2,1) = 0.d0 + (theg - 1.d0)*f0(2)*f0(1)
      fg(2,2) = 1.d0 + (theg - 1.d0)*f0(2)*f0(2)
      fg(2,3) = 0.d0 + (theg - 1.d0)*f0(2)*f0(3)
      fg(3,1) = 0.d0 + (theg - 1.d0)*f0(3)*f0(1)
      fg(3,2) = 0.d0 + (theg - 1.d0)*f0(3)*f0(2)
      fg(3,3) = 1.d0 + (theg - 1.d0)*f0(3)*f0(3)

      statev(4) = fg(1,1) * xn0(1) + fg(1,2) * xn0(2) + fg(1,3) * xn0(3)
      statev(5) = fg(2,1) * xn0(1) + fg(2,2) * xn0(2) + fg(2,3) * xn0(3)  
      statev(6) = fg(3,1) * xn0(1) + fg(3,2) * xn0(2) + fg(3,3) * xn0(3) 

      statev(4) = statev(4)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0
      statev(5) = statev(5)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0
      statev(6) = statev(6)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0

      fginv(1,1) = 1.d0 + (1.d0 - theg)/theg*f0(1)*f0(1)
      fginv(1,2) = 0.d0 + (1.d0 - theg)/theg*f0(1)*f0(2)
      fginv(1,3) = 0.d0 + (1.d0 - theg)/theg*f0(1)*f0(3)
      fginv(2,1) = 0.d0 + (1.d0 - theg)/theg*f0(2)*f0(1)
      fginv(2,2) = 1.d0 + (1.d0 - theg)/theg*f0(2)*f0(2)
      fginv(2,3) = 0.d0 + (1.d0 - theg)/theg*f0(2)*f0(3)
      fginv(3,1) = 0.d0 + (1.d0 - theg)/theg*f0(3)*f0(1)
      fginv(3,2) = 0.d0 + (1.d0 - theg)/theg*f0(3)*f0(2)
      fginv(3,3) = 1.d0 + (1.d0 - theg)/theg*f0(3)*f0(3)


c...  fiber growth
c...  calculate elastic tensor Fe = F + (1-theg)/theg * f \otimes f_0
      fe(1,1) = dfgrd1(1,1) + (1.d0 - theg)/theg * f(1)*f0(1)
      fe(1,2) = dfgrd1(1,2) + (1.d0 - theg)/theg * f(1)*f0(2)
      fe(1,3) = dfgrd1(1,3) + (1.d0 - theg)/theg * f(1)*f0(3)
      fe(2,1) = dfgrd1(2,1) + (1.d0 - theg)/theg * f(2)*f0(1)
      fe(2,2) = dfgrd1(2,2) + (1.d0 - theg)/theg * f(2)*f0(2)
      fe(2,3) = dfgrd1(2,3) + (1.d0 - theg)/theg * f(2)*f0(3)
      fe(3,1) = dfgrd1(3,1) + (1.d0 - theg)/theg * f(3)*f0(1)
      fe(3,2) = dfgrd1(3,2) + (1.d0 - theg)/theg * f(3)*f0(2)
      fe(3,3) = dfgrd1(3,3) + (1.d0 - theg)/theg * f(3)*f0(3)


      detfe = +fe(1,1) * (fe(2,2)*fe(3,3)-fe(2,3)*fe(3,2))
     #        -fe(1,2) * (fe(2,1)*fe(3,3)-fe(2,3)*fe(3,1))
     #        +fe(1,3) * (fe(2,1)*fe(3,2)-fe(2,2)*fe(3,1))

      lnJe = dlog(detfe)


      statev(17) = detfe
      statev(18) = fe(1,1)
      statev(19) = fe(1,2)
      statev(20) = fe(1,3)


c...  calculate elastic left cauchy-green deformation tensor be = fe * fe^t
      be(1) = fe(1,1)*fe(1,1) + fe(1,2)*fe(1,2) + fe(1,3)*fe(1,3)
      be(2) = fe(2,1)*fe(2,1) + fe(2,2)*fe(2,2) + fe(2,3)*fe(2,3)
      be(3) = fe(3,1)*fe(3,1) + fe(3,2)*fe(3,2) + fe(3,3)*fe(3,3)
      be(4) = fe(1,1)*fe(2,1) + fe(1,2)*fe(2,2) + fe(1,3)*fe(2,3)
      be(5) = fe(1,1)*fe(3,1) + fe(1,2)*fe(3,2) + fe(1,3)*fe(3,3)
      be(6) = fe(2,1)*fe(3,1) + fe(2,2)*fe(3,2) + fe(2,3)*fe(3,3)

      statev(21) = be(1)
      statev(22) = be(2)
      statev(23) = be(3)
      statev(24) = be(4)
      statev(25) = be(5)
      statev(26) = be(6)

c...  calculate left cauchy-green deformation tensor b = f * f^t
      b(1) = dfgrd1(1,1)*dfgrd1(1,1) + dfgrd1(1,2)*dfgrd1(1,2) + dfgrd1(1,3)*dfgrd1(1,3)
      b(2) = dfgrd1(2,1)*dfgrd1(2,1) + dfgrd1(2,2)*dfgrd1(2,2) + dfgrd1(2,3)*dfgrd1(2,3)
      b(3) = dfgrd1(3,1)*dfgrd1(3,1) + dfgrd1(3,2)*dfgrd1(3,2) + dfgrd1(3,3)*dfgrd1(3,3)
      b(4) = dfgrd1(1,1)*dfgrd1(2,1) + dfgrd1(1,2)*dfgrd1(2,2) + dfgrd1(1,3)*dfgrd1(2,3)
      b(5) = dfgrd1(1,1)*dfgrd1(3,1) + dfgrd1(1,2)*dfgrd1(3,2) + dfgrd1(1,3)*dfgrd1(3,3)
      b(6) = dfgrd1(2,1)*dfgrd1(3,1) + dfgrd1(2,2)*dfgrd1(3,2) + dfgrd1(2,3)*dfgrd1(3,3)

c...  calculate Cauchy stress
      do i = 1,ntens
        stress(i) = ((lam*lnJe-mu)*xi(i) + mu*be(i))/detfe
      end do


c...  calculate elastic and geometric tangent
cccc  ddsdde(1,1) = (lam + 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(1)
cccc  ddsdde(2,2) = (lam + 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(2)
cccc  ddsdde(3,3) = (lam + 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(3)
      ddsdde(1,1) = (lam - 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(1)
      ddsdde(2,2) = (lam - 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(2)
      ddsdde(3,3) = (lam - 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(3)
      ddsdde(1,2) = (lam)/detfe
      ddsdde(1,3) = (lam)/detfe
      ddsdde(2,3) = (lam)/detfe
      ddsdde(1,4) = stress(4)
      ddsdde(2,4) = stress(4)
      ddsdde(3,4) = 0.d0
cccc  ddsdde(4,4) = (lam*lnJe - mu)/detfe + (stress(1) + stress(2))/2.d0
      ddsdde(4,4) = -(lam*lnJe - mu)/detfe + (stress(1) + stress(2))/2.d0

      if (ntens.eq.6) then
        ddsdde(1,5) = stress(5)
        ddsdde(2,5) = 0.d0
        ddsdde(3,5) = stress(5)
        ddsdde(1,6) = 0.d0
        ddsdde(2,6) = stress(6)
        ddsdde(3,6) = stress(6)
cccc    ddsdde(5,5) = (lam*lnJe - mu)/detfe + (stress(1) + stress(3))/2.d0
cccc    ddsdde(6,6) = (lam*lnJe - mu)/detfe + (stress(2) + stress(3))/2.d0
        ddsdde(5,5) = -(lam*lnJe - mu)/detfe + (stress(1) + stress(3))/2.d0
        ddsdde(6,6) = -(lam*lnJe - mu)/detfe + (stress(2) + stress(3))/2.d0
        ddsdde(4,5) = stress(6)/2.d0
        ddsdde(4,6) = stress(5)/2.d0
        ddsdde(5,6) = stress(4)/2.d0
      end if

c...  use symmetry to fill in the rest
      do i = 2,ntens
        do j = 1,i-1
          ddsdde(i,j) = ddsdde(j,i)
        end do
      end do

c...  calculate growth tangent
cccc      cg_ij(1) = (lam*lnJe - lam - mu)*xi(1) + mu*xn(1)*xn(1)
cccc      cg_ij(2) = (lam*lnJe - lam - mu)*xi(2) + mu*xn(2)*xn(2)
cccc      cg_ij(3) = (lam*lnJe - lam - mu)*xi(3) + mu*xn(3)*xn(3)
cccc      cg_ij(4) = (lam*lnJe - lam - mu)*xi(4) + mu*xn(1)*xn(2)
cccc      cg_ij(5) = (lam*lnJe - lam - mu)*xi(5) + mu*xn(1)*xn(3)
cccc      cg_ij(6) = (lam*lnJe - lam - mu)*xi(6) + mu*xn(2)*xn(3)

      cg_ij(1) = -lam*xi(1) - 2*mu/the**2.d0*f(1)*f(1)
      cg_ij(2) = -lam*xi(2) - 2*mu/the**2.d0*f(2)*f(2)
      cg_ij(3) = -lam*xi(3) - 2*mu/the**2.d0*f(3)*f(3)
      cg_ij(4) = -lam*xi(4) - 2*mu/the**2.d0*f(1)*f(2)
      cg_ij(5) = -lam*xi(5) - 2*mu/the**2.d0*f(1)*f(3)
      cg_ij(6) = -lam*xi(6) - 2*mu/the**2.d0*f(2)*f(3)

      cg_kl(1) = f(1)*f(1)
      cg_kl(2) = f(2)*f(2)
      cg_kl(3) = f(3)*f(3)
      cg_kl(4) = f(1)*f(2)
      cg_kl(5) = f(1)*f(3)
      cg_kl(6) = f(2)*f(3)

c...  compile tangent
c      do i = 1,ntens
c        do j = 1,ntens
c          ddsdde(i,j) = ddsdde(i,j) + dtime*cg_ij(i)*cg_kl(j)/detfe**2.d0
c        end do
c      end do

c...  calculate strain energy
      sse = (lam*(lnJe)**2.d0 + mu*(be(1)+be(2)+be(3) - 3.d0 - 2.d0*lnJe))/2.d0
      statev(10) = sse
      return
      end

c...  ------------------------------------------------------------------

      subroutine cross(aa, bb,cc)
      implicit none

      real*8 :: cc(3)
      real*8 :: aa(3), bb(3)

      cc(1) = aa(2) * bb(3) - aa(3) * bb(2)
      cc(2) = aa(3) * bb(1) - aa(1) * bb(3)
      cc(3) = aa(1) * bb(2) - aa(2) * bb(1)

      return
      end
c...  ------------------------------------------------------------------

c...  ------------------------------------------------------------------
c...  Subroutine for moving 6x1 tensor to 3x3 tensor
c...  ------------------------------------------------------------------
      subroutine move6to33(a6,a33)
      implicit none
      real*8   a6(6),a33(3,3)

      a33(1,1) = a6(1)
      a33(2,2) = a6(2)
      a33(3,3) = a6(3)
      a33(1,2) = a6(4)
      a33(1,3) = a6(5)
      a33(2,3) = a6(6)
      a33(2,1) = a33(1,2)
      a33(3,1) = a33(1,3)
      a33(3,2) = a33(2,3)

      return
      end

c...  ------------------------------------------------------------------
c...  Subroutine for outer product 3x1 tensor by 3x1 tensor
c...  ------------------------------------------------------------------
      subroutine outer3by3(a3,b3,c33)
      implicit none
      real*8   a3(3),b3(3),c33(3,3)

      c33(1,1) = a3(1)*b3(1)
      c33(2,2) = a3(1)*b3(2)
      c33(3,3) = a3(1)*b3(3)
      c33(1,2) = a3(2)*b3(1)
      c33(1,3) = a3(2)*b3(2)
      c33(2,3) = a3(2)*b3(3)
      c33(2,1) = a3(3)*b3(1)
      c33(3,1) = a3(3)*b3(2)
      c33(3,2) = a3(3)*b3(3)

      return
      end