c...  ------------------------------------------------------------------
      subroutine sdvini(statev,coords,nstatv,ncrds,noel,npt,layer,kspt)
c...  ------------------------------------------------------------------
      include 'aba_param.inc'

      dimension statev(nstatv)

      statev(1)=1.0d0    ! the_g
      statev(2)=1.0d0    ! the_e
      statev(3)=1.0d0    ! the
      statev(4)=1.0d0    ! xn(1)
      statev(5)=1.0d0    ! xn(2)
      statev(6)=1.0d0    ! xn(3)
      statev(7)=1.0d0    ! x_coordinate in reference
      statev(8)=1.0d0    ! y_coordinate in reference
      statev(9)=1.0d0    ! z_coordinate in reference
      statev(10)=0.0d0    ! transformed stress(1,1)_rr
      statev(11)=0.0d0    ! transformed stress(1,2) 
      statev(12)=0.0d0    ! transformed stress(1,3) 
      statev(13)=0.0d0    ! transformed stress(2,1)
      statev(14)=0.0d0    ! transformed stress(2,2) theta circumferential
      statev(15)=0.0d0    ! transformed stress(2,3)
      statev(16)=0.0d0    ! transformed stress(3,1)
      statev(17)=0.0d0    ! transformed stress(3,2)
      statev(18)=0.0d0    ! transformed stress(3,3) phi hoop
      statev(19)=1.0d0    ! (3,1) of F
      statev(20)=1.0d0    ! (3,2) of F
      statev(21)=1.0d0    ! (3,3) of F
      statev(22)=1.0d0    ! pk2stress*hoop(1)
      statev(23)=1.0d0    ! pk2stress*hoop(2)
      statev(24)=1.0d0    ! pk2stress*hoop(3)
      statev(25)=1.0d0    ! magnitude of pk2stress*hoop
      statev(26)=1.0d0    ! pk2stress*axial(1)
      statev(27)=1.0d0    ! pk2stress*axial(2)
      statev(28)=1.0d0    ! pk2stress*axial(3)
      statev(29)=1.0d0    ! magnitude of pk2stress*axial
      statev(30)=1.0d0    ! pk2stress*radial(1)
      statev(31)=1.0d0    ! pk2stress*radial(2)
      statev(32)=1.0d0    ! pk2stress*radial(3)
      statev(33)=1.0d0    ! magnitude of pk2stress*radial
      statev(34)=1.0d0    ! strain energy
      statev(35)=1.0d0    ! hoop(1)
      statev(36)=1.0d0    ! hoop(2)
      statev(37)=1.0d0    ! hoop(3)
      statev(38)=1.0d0    ! circumferential(1)
      statev(39)=1.0d0    ! circumferential(2)
      statev(40)=1.0d0    ! circumferential(3)

      statev(41)=1.0d0    ! (1,1) of F
      statev(42)=1.0d0    ! (1,2) of F
      statev(43)=1.0d0    ! (1,3) of F
      statev(44)=1.0d0    ! (2,1) of F
      statev(45)=1.0d0    ! (2,2) of F
      statev(46)=1.0d0    ! (2,3) of F
      statev(47)=1.0d0    ! (3,1) of F
      statev(48)=1.0d0    ! (3,2) of F
      statev(49)=1.0d0    ! (3,3) of F

      statev(50)=1.0d0    ! (1,1) of Fg
      statev(51)=1.0d0    ! (1,2) of Fg
      statev(52)=1.0d0    ! (1,3) of Fg
      statev(53)=1.0d0    ! (2,1) of Fg
      statev(54)=1.0d0    ! (2,2) of Fg
      statev(55)=1.0d0    ! (2,3) of Fg
      statev(56)=1.0d0    ! (3,1) of Fg
      statev(57)=1.0d0    ! (3,2) of Fg
      statev(58)=1.0d0    ! (3,3) of Fg

      statev(59)=1.0d0    ! (1,1) of Fe
      statev(60)=1.0d0    ! (1,2) of Fe
      statev(61)=1.0d0    ! (1,3) of Fe
      statev(62)=1.0d0    ! (2,1) of Fe
      statev(63)=1.0d0    ! (2,2) of Fe
      statev(64)=1.0d0    ! (2,3) of Fe
      statev(65)=1.0d0    ! (3,1) of Fe
      statev(66)=1.0d0    ! (3,2) of Fe
      statev(67)=1.0d0    ! (3,3) of Fe



      return
      end



c...  ------------------------------------------------------------------
c     user amplitude subroutine
      Subroutine UAMP(
     *     ampName, time, ampValueOld, dt, nProps, props, nSvars, svars,
     *     lFlagsInfo, nSensor, sensorValues, sensorNames,
     *     jSensorLookUpTable,
C          to be defined
     *     ampValueNew,
     *     lFlagsDefine,
     *     AmpDerivative, AmpSecDerivative, AmpIncIntegral,
     *     AmpIncDoubleIntegral)

      include 'aba_param.inc'

C     svars - additional state variables, similar to (V)UEL

C     time indices
      parameter (iStepTime        = 1,
     *           iTotalTime       = 2,
     *           nTime            = 2)
C     flags passed in for information
      parameter (iInitialization   = 1,
     *           iRegularInc       = 2,
     *           nFlagsInfo        = 2)
C     optional flags to be defined
      parameter (iComputeDeriv       = 1,
     *           iComputeSecDeriv    = 2,
     *           iComputeInteg       = 3,
     *           iComputeDoubleInteg = 4,
     *           iStopAnalysis       = 5,
     *           iConcludeStep       = 6,
     *           nFlagsDefine        = 6)

      dimension time(nTime), lFlagsInfo(nFlagsInfo),
     *          lFlagsDefine(nFlagsDefine)
      dimension jSensorLookUpTable(*)
      common /pressure/ porepres
      common /porepres_pre/ porepres_pre
      common /volume/ porevol
      common /preKiterm/ preKiterm
      common /preerr/ preerr
      common /tStart_before/ tStart_before

      double precision porepres,porevol,preKiterm,
     #                 preerr,tStart_before,porepres_pre
      dimension sensorValues(nSensor), svars(nSvars), props(nProps)

      character*80 sensorNames(nSensor)
      character*80 ampName

c     get sensor values first
      porevol = GetSensorValue('VOL_EXP',jSensorLookUpTable,
     *                           sensorValues)
      porepres_pre = GetSensorValue('PRES_EXP',jSensorLookUpTable,
     *                           sensorValues)
c      ampValueOld = 0.008
      ampValueOld = 0.0001


      if (ampName(1:7) .eq. 'AMP_PID' ) then

C        User code to compute  ampValue = F(sensors)
         if (lFlagsInfo(iInitialization).eq.1) then
c	    tim = time(iStepTime)
            tStart_before = time(iStepTime)
            ampValueNew  = ampValueOld
            porepres = ampValueOld

         else
c           Example: f(t) = t

            tim = time(iStepTime)
            tStart = tim - dt
            tEnd   = tim
            call pid_control(dt,tim,tStart)
            ampValueNew = porepres


C           stop the analysis, if desired
            if (rfAve.gt.1000000) lFlagsDefine(iStopAnalysis)=1
         end if

      end if

      return
      end

c...  ------------------------------------------------------------------
      subroutine pid_control(dt,tim,tStart)
      implicit none
      common /pressure/ porepres
      common /porepres_pre/ porepres_pre
      common /volume/ porevol
      common /preKiterm/ preKiterm
      common /preerr/ preerr
      common /tStart_before/ tStart_before


      real*8  tim,tStart, tStart_before
      real*8  prevol, curvol, err, SP, Kp, Ki, Kd, dt
      real*8  output,porepres,porevol,preKiterm,preerr,porepres_pre


      curvol = porevol


      SP = 69000.d0

      Kp = 1.0d0
      Ki = 0.5d0
      Kd = 0.01d0


      err = SP - curvol

      if (tStart_before.eq.tStart) then
        porevol = porevol
        tStart_before = tStart
      else

        output = Kp*err + preKiterm + Ki*dt + Kd*(err-preerr)/dt


        if (tim.gt.0.02d0) then
          porepres= porepres_pre + 10.d0*output/60000000.d0 !new pres for 30cc
c          porepres= porepres_pre + 7.d0*output/100000000.d0	
        else
          porepres= porepres_pre + output/70000000.d0 !new pres for 30cc

        end if
ccccccccccccccccc
ccccccccccccccccc
        porevol = curvol + output ! new volume
        preKiterm = preKiterm + Ki*dt ! new Ki term
        preerr = err ! new error term


        tStart_before = tStart

      end if


      RETURN
      END
c...  ------------------------------------------------------------------


c...  ------------------------------------------------------------------
      subroutine umat(stress,statev,ddsdde,sse,spd,scd,
     #rpl,ddsddt,drplde,drpldt,
     #stran,dstran,time,dtime,temp,dtemp,predef,dpred,cmname,
     #ndi,nshr,ntens,nstatv,props,nprops,coords,drot,pnewdt,
     #celent,dfgrd0,dfgrd1,noel,npt,layer,kspt,kstep,kinc)
c...  ------------------------------------------------------------------
      include 'aba_param.inc'

      character*80 cmname
      dimension stress(ntens),statev(nstatv),
     #ddsdde(ntens,ntens),ddsddt(ntens),drplde(ntens),
     #stran(ntens),dstran(ntens),time(2),predef(1),dpred(1),
     #props(nprops),coords(3),drot(3,3),dfgrd0(3,3),dfgrd1(3,3)

      call umat_area_stretch(stress,statev,ddsdde,sse,
     #                       time,dtime,coords,props,dfgrd1,
     #                       ntens,ndi,nshr,nstatv,nprops,
     #                       noel,npt,kstep,kinc)

      return
      end

c...  ------------------------------------------------------------------
      subroutine umat_area_stretch(stress,statev,ddsdde,sse,
     #                             time,dtime,coords,props,dfgrd1,
     #                             ntens,ndi,nshr,nstatv,nprops,
     #                             noel,npt,kstep,kinc)
c...  ------------------------------------------------------------------
c * " This UMAT is written for a Neohookean constitutive material.
c * " It implements area-stretch-driven area growth in the plane normal to xn0.
c * " It is not suitable for use with local material directions.

c * " Written by Maria Holland in Aug 2012
c * " Updated by Maria Holland in Sep 2017
c * " Edited by Taeksang Lee in Mar 2019
c...  ------------------------------------------------------------------

      implicit none

c...  variables to be defined
      real*8  stress(ntens), ddsdde(ntens,ntens), statev(nstatv), sse

c...  variables passed in for information
      real*8  time(2), dtime, coords(3), props(nprops), dfgrd1(3,3)
      integer ntens, ndi, nshr, nstatv, nprops, noel, npt, kstep, kinc

c...  material properties
      real*8  lam, mu, xn0(3), hill2, tcr, hill3, hill1

c...  local variables
      integer i, j, nitl
      real*8  norm, xn(3), finv(3,3), ftn0(3), phi, theta, R
      real*8  the, theg, theg_n, detf,n0(3),stress_temp(6)
      real*8  fe(3,3), fg(3,3),be(6), b(6), detfe, lnJe
      real*8  phig, res, dres, dot_theg_dtheg, dot_theg, coords_g(3)
      real*8  fac, cg_ij(6), cg_kl(6), Amat(3,3), Bmat(3,3), stressmat(3,3)
      real*8  xi(6), xtol, pi, mat1(3,3), mat2(3,3)
      real*8 axis_y(3), hoop_g(3), hoop_g_temp(3)
      real*8 feinv(3,3), stressmat_ori(3,3), pk2emat_corct(3,3),rad_g(3),cirf_g_temp(3),cirf_g(3)
      real*8 pk2etmp(3,3),pk2emat(3,3)

      data xi/1.d0,1.d0,1.d0,0.d0,0.d0,0.d0/
      xtol = 1.d-12
      pi = 3.1415927

c...  initialize material parameters
      lam   = props(1)    ! lame constant
      mu    = props(2)    ! shear modulus
      xn0(1) = props(3)   ! n0[1]
      xn0(2) = props(4)   ! n0[2]
      xn0(3) = props(5)   ! n0[3]
      hill2 = props(6)    ! growth rate para1
      tcr   = props(7)    ! critical stretch
      hill3 = props(8)        ! growth rate para2
      hill1   = props(9)    ! exponential growth rate

c...  save coordinate at integration point in reference configuration
      if (kinc.eq.1) then
        statev(7) = coords(1)
        statev(8) = coords(2)
        statev(9) = coords(3)
      end if

c      print *, 'time(1),time(2):', time(1),time(2)

c      statev(10) = dfgrd1(1,1)
c      statev(11) = dfgrd1(1,2)
c      statev(12) = dfgrd1(1,3)
c      statev(13) = dfgrd1(1,1)
c      statev(14) = dfgrd1(1,2)
c      statev(15) = dfgrd1(1,3)
c      statev(16) = dfgrd1(2,1)
c      statev(17) = dfgrd1(2,2)
c      statev(18) = dfgrd1(2,3)
      statev(19) = dfgrd1(3,1)
      statev(20) = dfgrd1(3,2)
      statev(21) = dfgrd1(3,3)

c...  calculate deformed elastic normal
cccc  norm = sqrt(xn0(1)*xn0(1) + xn0(2)*xn0(2) + xn0(3)*xn0(3))
cccc  xn(1) = (dfgrd1(1,1)*xn0(1) + dfgrd1(1,2)*xn0(2) + dfgrd1(1,3)*xn0(3))/norm
cccc  xn(2) = (dfgrd1(2,1)*xn0(1) + dfgrd1(2,2)*xn0(2) + dfgrd1(2,3)*xn0(3))/norm
cccc  xn(3) = (dfgrd1(3,1)*xn0(1) + dfgrd1(3,2)*xn0(2) + dfgrd1(3,3)*xn0(3))/norm
cccc  statev(4) = xn(1)
cccc  statev(5) = xn(2)
cccc  statev(6) = xn(3)

      xn(1) = (dfgrd1(1,1)*xn0(1) + dfgrd1(1,2)*xn0(2) + dfgrd1(1,3)*xn0(3))
      xn(2) = (dfgrd1(2,1)*xn0(1) + dfgrd1(2,2)*xn0(2) + dfgrd1(2,3)*xn0(3))
      xn(3) = (dfgrd1(3,1)*xn0(1) + dfgrd1(3,2)*xn0(2) + dfgrd1(3,3)*xn0(3))
      norm = sqrt(xn(1)*xn(1) + xn(2)*xn(2) + xn(3)*xn(3))
C      statev(4) = xn(1)/norm
c      statev(5) = xn(2)/norm
c      statev(6) = xn(3)/norm

c...  calculate determinant of deformation gradient
      detf = +dfgrd1(1,1)*(dfgrd1(2,2)*dfgrd1(3,3)-dfgrd1(2,3)*dfgrd1(3,2))
     #       -dfgrd1(1,2)*(dfgrd1(2,1)*dfgrd1(3,3)-dfgrd1(2,3)*dfgrd1(3,1))
     #       +dfgrd1(1,3)*(dfgrd1(2,1)*dfgrd1(3,2)-dfgrd1(2,2)*dfgrd1(3,1))

c...  calculate area stretch
      finv(1,1) = (+dfgrd1(2,2)*dfgrd1(3,3) - dfgrd1(2,3)*dfgrd1(3,2))/detf
      finv(1,2) = (-dfgrd1(1,2)*dfgrd1(3,3) + dfgrd1(1,3)*dfgrd1(3,2))/detf
      finv(1,3) = (+dfgrd1(1,2)*dfgrd1(2,3) - dfgrd1(1,3)*dfgrd1(2,2))/detf
      finv(2,1) = (-dfgrd1(2,1)*dfgrd1(3,3) + dfgrd1(2,3)*dfgrd1(3,1))/detf
      finv(2,2) = (+dfgrd1(1,1)*dfgrd1(3,3) - dfgrd1(1,3)*dfgrd1(3,1))/detf
      finv(2,3) = (-dfgrd1(1,1)*dfgrd1(2,3) + dfgrd1(1,3)*dfgrd1(2,1))/detf
      finv(3,1) = (+dfgrd1(2,1)*dfgrd1(3,2) - dfgrd1(2,2)*dfgrd1(3,1))/detf
      finv(3,2) = (-dfgrd1(1,1)*dfgrd1(3,2) + dfgrd1(1,2)*dfgrd1(3,1))/detf
      finv(3,3) = (+dfgrd1(1,1)*dfgrd1(2,2) - dfgrd1(1,2)*dfgrd1(2,1))/detf

      ftn0(1) = finv(1,1)*xn0(1)+finv(2,1)*xn0(2)+finv(3,1)*xn0(3)
      ftn0(2) = finv(1,2)*xn0(1)+finv(2,2)*xn0(2)+finv(3,2)*xn0(3)
      ftn0(3) = finv(1,3)*xn0(1)+finv(2,3)*xn0(2)+finv(3,3)*xn0(3)

      the = detf

c...  obtain state variable history
      theg_n = statev(1)
      theg   = theg_n


c...  update state variables
c     linear field  
c      if (kinc > 2) then
c        theg = theg + dtime/1.d0*1.d0/4.d0
c     #         *sqrt(statev(7)**2.d0+statev(8)**2.d0+statev(9)**2.d0)
c      end if

c     square root field-not used
c      if (kinc > 2) then
c        theg = theg + dtime/1.d0*1.d0/4.d0
c     #    *(statev(7)**2.d0+statev(8)**2.d0+statev(9)**2.d0)**0.25d0
c      end if

c     sharp increase field
c      if (kinc > 2) then
c        theg = theg+ dtime/1.d0*(0.25d0 - 0.25d0
c     #  *(sqrt(statev(7)**2.d0+statev(8)**2.d0+statev(9)**2.d0)-1.d0)**4.d0)
c      end if

c     slow increase field
      if (kinc > 2) then
        theg = theg + dtime/1.d0*1.d0/4.d0
     #    *(statev(7)**2.d0+statev(8)**2.d0+statev(9)**2.d0)**2.d0
      end if


      statev(1) = theg
      statev(2) = the/theg
      statev(3) = the
      
c      print *, 'noel,npt,time(1),coordx_ref_curr:', noel,npt,time(1),statev(7),coords(1)

c...  volume isotropic volumetric-stretch-driven growth
c...  calculate growth tensor Fg = theg^(1/3) * I
      fg(1,1) = theg**(1.d0/3.d0)
      fg(1,2) = 0.d0
      fg(1,3) = 0.d0
      fg(2,1) = 0.d0
      fg(2,2) = theg**(1.d0/3.d0)
      fg(2,3) = 0.d0
      fg(3,1) = 0.d0
      fg(3,2) = 0.d0
      fg(3,3) = theg**(1.d0/3.d0)

      n0(1) = statev(7)/(statev(7)**2.d0+statev(8)**2.d0+statev(9)**2.d0)**0.5d0
      n0(2) = statev(8)/(statev(7)**2.d0+statev(8)**2.d0+statev(9)**2.d0)**0.5d0
      n0(3) = statev(9)/(statev(7)**2.d0+statev(8)**2.d0+statev(9)**2.d0)**0.5d0
 
      statev(4) = fg(1,1) * n0(1) 
      statev(5) = fg(2,2) * n0(2) 
      statev(6) = fg(3,3) * n0(3) 

      statev(4) = statev(4)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0
      statev(5) = statev(5)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0
      statev(6) = statev(6)/(statev(4)**2.d0+statev(5)**2.d0+statev(6)**2.d0)**0.5d0
      

c...  volume isotropic volumetric-stretch-driven growth
c...  calculate elastic tensor Fe = 1\theg^(1/3) * F
      fe(1,1) = dfgrd1(1,1)/(theg**(1.d0/3.d0))
      fe(1,2) = dfgrd1(1,2)/(theg**(1.d0/3.d0))
      fe(1,3) = dfgrd1(1,3)/(theg**(1.d0/3.d0))
      fe(2,1) = dfgrd1(2,1)/(theg**(1.d0/3.d0))
      fe(2,2) = dfgrd1(2,2)/(theg**(1.d0/3.d0))
      fe(2,3) = dfgrd1(2,3)/(theg**(1.d0/3.d0))
      fe(3,1) = dfgrd1(3,1)/(theg**(1.d0/3.d0))
      fe(3,2) = dfgrd1(3,2)/(theg**(1.d0/3.d0))
      fe(3,3) = dfgrd1(3,3)/(theg**(1.d0/3.d0))

c...  calculate elastic tensor Fe = 1\sqrt(theg)*F + (1-1/sqrt(theg))*n \otimes n_0
c      fe(1,1) = dfgrd1(1,1)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(1)*xn0(1)
c      fe(1,2) = dfgrd1(1,2)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(1)*xn0(2)
c      fe(1,3) = dfgrd1(1,3)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(1)*xn0(3)
c      fe(2,1) = dfgrd1(2,1)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(2)*xn0(1)
c      fe(2,2) = dfgrd1(2,2)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(2)*xn0(2)
c      fe(2,3) = dfgrd1(2,3)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(2)*xn0(3)
c      fe(3,1) = dfgrd1(3,1)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(3)*xn0(1)
c      fe(3,2) = dfgrd1(3,2)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(3)*xn0(2)
c      fe(3,3) = dfgrd1(3,3)/sqrt(theg) + (1.d0 - 1.d0/sqrt(theg))*xn(3)*xn0(3)

      detfe = +fe(1,1) * (fe(2,2)*fe(3,3)-fe(2,3)*fe(3,2))
     #        -fe(1,2) * (fe(2,1)*fe(3,3)-fe(2,3)*fe(3,1))
     #        +fe(1,3) * (fe(2,1)*fe(3,2)-fe(2,2)*fe(3,1))
c      print *, 'time(2),noel,dres,res,dot_theg_dtheg', time(2),noel,dres,res,dot_theg_dtheg
      lnJe = dlog(detfe)


      statev(41)=dfgrd1(1,1)
      statev(42)=dfgrd1(1,2)
      statev(43)=dfgrd1(1,3)
      statev(44)=dfgrd1(2,1)
      statev(45)=dfgrd1(2,2)
      statev(46)=dfgrd1(2,3)
      statev(47)=dfgrd1(3,1)
      statev(48)=dfgrd1(3,2)
      statev(49)=dfgrd1(3,3)

      statev(50)=fg(1,1)
      statev(51)=fg(1,2)
      statev(52)=fg(1,3)
      statev(53)=fg(2,1)
      statev(54)=fg(2,2)
      statev(55)=fg(2,3)
      statev(56)=fg(3,1)
      statev(57)=fg(3,2)
      statev(58)=fg(3,3)

      statev(59)=fe(1,1)
      statev(60)=fe(1,2)
      statev(61)=fe(1,3)
      statev(62)=fe(2,1)
      statev(63)=fe(2,2)
      statev(64)=fe(2,3)
      statev(65)=fe(3,1)
      statev(66)=fe(3,2)
      statev(67)=fe(3,3)


c...  calculate elastic left cauchy-green deformation tensor be = fe * fe^t
      be(1) = fe(1,1)*fe(1,1) + fe(1,2)*fe(1,2) + fe(1,3)*fe(1,3)
      be(2) = fe(2,1)*fe(2,1) + fe(2,2)*fe(2,2) + fe(2,3)*fe(2,3)
      be(3) = fe(3,1)*fe(3,1) + fe(3,2)*fe(3,2) + fe(3,3)*fe(3,3)
      be(4) = fe(1,1)*fe(2,1) + fe(1,2)*fe(2,2) + fe(1,3)*fe(2,3)
      be(5) = fe(1,1)*fe(3,1) + fe(1,2)*fe(3,2) + fe(1,3)*fe(3,3)
      be(6) = fe(2,1)*fe(3,1) + fe(2,2)*fe(3,2) + fe(2,3)*fe(3,3)

c...  calculate left cauchy-green deformation tensor b = f * f^t
      b(1) = dfgrd1(1,1)*dfgrd1(1,1) + dfgrd1(1,2)*dfgrd1(1,2) + dfgrd1(1,3)*dfgrd1(1,3)
      b(2) = dfgrd1(2,1)*dfgrd1(2,1) + dfgrd1(2,2)*dfgrd1(2,2) + dfgrd1(2,3)*dfgrd1(2,3)
      b(3) = dfgrd1(3,1)*dfgrd1(3,1) + dfgrd1(3,2)*dfgrd1(3,2) + dfgrd1(3,3)*dfgrd1(3,3)
      b(4) = dfgrd1(1,1)*dfgrd1(2,1) + dfgrd1(1,2)*dfgrd1(2,2) + dfgrd1(1,3)*dfgrd1(2,3)
      b(5) = dfgrd1(1,1)*dfgrd1(3,1) + dfgrd1(1,2)*dfgrd1(3,2) + dfgrd1(1,3)*dfgrd1(3,3)
      b(6) = dfgrd1(2,1)*dfgrd1(3,1) + dfgrd1(2,2)*dfgrd1(3,2) + dfgrd1(2,3)*dfgrd1(3,3)

c...  calculate Cauchy stress
      do i = 1,ntens
        stress(i) = ((lam*lnJe-mu)*xi(i) + mu*be(i))/detfe
      end do

      call move6to33(stress,stressmat_ori)

      feinv = theg**(1.d0/3.d0) * finv
      pk2etmp = matmul(feinv,stressmat_ori)
      pk2emat = detfe * matmul(pk2etmp,transpose(feinv))

c...  for hoop direction
      coords_g(1) = fg(1,1) * statev(7) 
      coords_g(2) = fg(2,2) * statev(8)
      coords_g(3) = fg(3,3) * statev(9)

      axis_y(1) = 0.d0
      axis_y(2) = 0.d0
      axis_y(3) = 1.d0
      call cross(axis_y,coords_g,hoop_g_temp)
      hoop_g = hoop_g_temp/sqrt(hoop_g_temp(1)**2.d0 + hoop_g_temp(2)**2.d0 + hoop_g_temp(3)**2.d0)
      statev(22) = pk2emat(1,1)*hoop_g(1) + pk2emat(1,2)*hoop_g(2) + pk2emat(1,3)*hoop_g(3)
      statev(23) = pk2emat(2,1)*hoop_g(1) + pk2emat(2,2)*hoop_g(2) + pk2emat(2,3)*hoop_g(3)
      statev(24) = pk2emat(3,1)*hoop_g(1) + pk2emat(3,2)*hoop_g(2) + pk2emat(3,3)*hoop_g(3)
      statev(25) = sqrt(statev(22)**2.d0 + statev(23)**2.d0 + statev(24)**2.d0)


c...  for circumferentail direction
      call cross(coords_g,hoop_g,cirf_g_temp)
      cirf_g = cirf_g_temp / sqrt(cirf_g_temp(1)**2.d0 + cirf_g_temp(2)**2.d0 + cirf_g_temp(3)**2.d0)
      statev(26) = pk2emat(1,1)*cirf_g(1) + pk2emat(1,2)*cirf_g(2) + pk2emat(1,3)*cirf_g(3)
      statev(27) = pk2emat(2,1)*cirf_g(1) + pk2emat(2,2)*cirf_g(2) + pk2emat(2,3)*cirf_g(3)
      statev(28) = pk2emat(3,1)*cirf_g(1) + pk2emat(3,2)*cirf_g(2) + pk2emat(3,3)*cirf_g(3)
      statev(29) = sqrt(statev(26)**2.d0 + statev(27)**2.d0 + statev(28)**2.d0)

 
c...  for radial direction
      rad_g = coords_g / sqrt(coords_g(1)**2.d0 + coords_g(2)**2.d0 + coords_g(3)**2.d0)
      statev(30) = pk2emat(1,1)*rad_g(1) + pk2emat(1,2)*rad_g(2) + pk2emat(1,3)*rad_g(3)
      statev(31) = pk2emat(2,1)*rad_g(1) + pk2emat(2,2)*rad_g(2) + pk2emat(2,3)*rad_g(3)
      statev(32) = pk2emat(3,1)*rad_g(1) + pk2emat(3,2)*rad_g(2) + pk2emat(3,3)*rad_g(3)
      statev(33) = sqrt(statev(30)**2.d0 + statev(31)**2.d0 + statev(32)**2.d0)


      R = sqrt(coords_g(1)**2.d0 + coords_g(2)**2.d0 + coords_g(3)**2.d0)

      theta = acos(coords_g(3)/R)
      phi = atan(abs(coords_g(2)/coords_g(1)))

      if (coords_g(1).gt.0.d0 .and. coords_g(2).gt.0.d0) then

        phi = phi

      else if (coords_g(1).gt.0.d0 .and. coords_g(2).lt.0.d0) then
        phi = 2.d0*pi - phi

      else if (coords_g(1).lt.0.d0 .and. coords_g(2).lt.0.d0) then
        phi = pi + phi

      else if (coords_g(1).lt.0.d0 .and. coords_g(2).gt.0.d0) then
        phi = pi - phi

      end if

      Amat(1,1) = sin(theta)*cos(phi)
      Amat(1,2) = sin(theta)*sin(phi)
      Amat(1,3) = cos(theta)

      Amat(2,1) = cos(theta)*cos(phi)
      Amat(2,2) = cos(theta)*sin(phi)
      Amat(2,3) = -sin(theta)

      Amat(3,1) = -sin(phi)
      Amat(3,2) = cos(phi)
      Amat(3,3) = 0.d0

      Bmat(1,1) = sin(theta)*cos(phi)
      Bmat(1,2) = cos(theta)*cos(phi)
      Bmat(1,3) = -sin(phi)

      Bmat(2,1) = sin(theta)*sin(phi)
      Bmat(2,2) = cos(theta)*sin(phi)
      Bmat(2,3) = cos(phi)

      Bmat(3,1) = cos(theta)
      Bmat(3,2) = -sin(theta)
      Bmat(3,3) = 0.d0
      mat1 = matmul(Amat,pk2emat)
      mat2 = matmul(mat1,Bmat)
      statev(10) = mat2(1,1) ! stress_r
      statev(11) = mat2(1,2) ! r_theta
      statev(12) = mat2(1,3) ! r_phi
      statev(13) = mat2(2,1) ! theta_r
      statev(14) = mat2(2,2) ! stress_theta
      statev(15) = mat2(2,3) ! theta_phi
      statev(16) = mat2(3,1) ! phi_r
      statev(17) = mat2(3,2) ! phi_theta
      statev(18) = mat2(3,3) ! stress_phi

c      print *, 'F',statev(13),statev(14),statev(15),statev(16),statev(17),statev(18),statev(19),statev(20),statev(21)
c      print *, 'comp',coords_g(1),coords_g(2),coords_g(3), phi,theta, R
c      print *, 'stre',coords_g(1),stress_temp(1),stress_temp(2),stress_temp(3),stress_temp(4),stress_temp(5),stress_temp(6)
c      print *, 'stat',coords_g(1),statev(10),statev(11),statev(12)



c...  calculate elastic and geometric tangent
cccc  ddsdde(1,1) = (lam + 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(1)
cccc  ddsdde(2,2) = (lam + 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(2)
cccc  ddsdde(3,3) = (lam + 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(3)
      ddsdde(1,1) = (lam - 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(1)
      ddsdde(2,2) = (lam - 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(2)
      ddsdde(3,3) = (lam - 2.d0*(lam*lnJe - mu))/detfe + 2.d0*stress(3)
      ddsdde(1,2) = (lam)/detfe
      ddsdde(1,3) = (lam)/detfe
      ddsdde(2,3) = (lam)/detfe
      ddsdde(1,4) = stress(4)
      ddsdde(2,4) = stress(4)
      ddsdde(3,4) = 0.d0
cccc  ddsdde(4,4) = (lam*lnJe - mu)/detfe + (stress(1) + stress(2))/2.d0
      ddsdde(4,4) = -(lam*lnJe - mu)/detfe + (stress(1) + stress(2))/2.d0

      if (ntens.eq.6) then
        ddsdde(1,5) = stress(5)
        ddsdde(2,5) = 0.d0
        ddsdde(3,5) = stress(5)
        ddsdde(1,6) = 0.d0
        ddsdde(2,6) = stress(6)
        ddsdde(3,6) = stress(6)
cccc    ddsdde(5,5) = (lam*lnJe - mu)/detfe + (stress(1) + stress(3))/2.d0
cccc    ddsdde(6,6) = (lam*lnJe - mu)/detfe + (stress(2) + stress(3))/2.d0
        ddsdde(5,5) = -(lam*lnJe - mu)/detfe + (stress(1) + stress(3))/2.d0
        ddsdde(6,6) = -(lam*lnJe - mu)/detfe + (stress(2) + stress(3))/2.d0
        ddsdde(4,5) = stress(6)/2.d0
        ddsdde(4,6) = stress(5)/2.d0
        ddsdde(5,6) = stress(4)/2.d0
      end if

c...  use symmetry to fill in the rest
      do i = 2,ntens
        do j = 1,i-1
          ddsdde(i,j) = ddsdde(j,i)
        end do
      end do

c...  calculate growth tangent
cccc      cg_ij(1) = (lam*lnJe - lam - mu)*xi(1) + mu*xn(1)*xn(1)
cccc      cg_ij(2) = (lam*lnJe - lam - mu)*xi(2) + mu*xn(2)*xn(2)
cccc      cg_ij(3) = (lam*lnJe - lam - mu)*xi(3) + mu*xn(3)*xn(3)
cccc      cg_ij(4) = (lam*lnJe - lam - mu)*xi(4) + mu*xn(1)*xn(2)
cccc      cg_ij(5) = (lam*lnJe - lam - mu)*xi(5) + mu*xn(1)*xn(3)
cccc      cg_ij(6) = (lam*lnJe - lam - mu)*xi(6) + mu*xn(2)*xn(3)

      cg_ij(1) = lam*theg*xi(1) + mu*(b(1)-xn(1)*xn(1))
      cg_ij(2) = lam*theg*xi(2) + mu*(b(2)-xn(2)*xn(2))
      cg_ij(3) = lam*theg*xi(3) + mu*(b(3)-xn(3)*xn(3))
      cg_ij(4) = lam*theg*xi(4) + mu*(b(4)-xn(1)*xn(2))
      cg_ij(5) = lam*theg*xi(5) + mu*(b(5)-xn(1)*xn(3))
      cg_ij(6) = lam*theg*xi(6) + mu*(b(6)-xn(2)*xn(3))

      cg_kl(1) = the*xi(1) - detf*detf/the*ftn0(1)*ftn0(1)
      cg_kl(2) = the*xi(2) - detf*detf/the*ftn0(2)*ftn0(2)
      cg_kl(3) = the*xi(3) - detf*detf/the*ftn0(3)*ftn0(3)
      cg_kl(4) = the*xi(4) - detf*detf/the*ftn0(1)*ftn0(2)
      cg_kl(5) = the*xi(5) - detf*detf/the*ftn0(1)*ftn0(3)
      cg_kl(6) = the*xi(6) - detf*detf/the*ftn0(2)*ftn0(3)

c...  compile tangent
c      do i = 1,ntens
c        do j = 1,ntens
c          ddsdde(i,j) = ddsdde(i,j) + fac*cg_ij(i)*cg_kl(j)/detfe
c        end do
c      end do

c...  calculate strain energy
      sse = (lam*(lnJe)**2.d0 + mu*(be(1)+be(2)+be(3) - 3.d0 - 2.d0*lnJe))/2.d0
      statev(34)=sse
      return
      end

c...  ------------------------------------------------------------------

      subroutine cross(aa, bb,cc)
      implicit none

      real*8 :: cc(3)
      real*8 :: aa(3), bb(3)

      cc(1) = aa(2) * bb(3) - aa(3) * bb(2)
      cc(2) = aa(3) * bb(1) - aa(1) * bb(3)
      cc(3) = aa(1) * bb(2) - aa(2) * bb(1)

      return
      end
c...  ------------------------------------------------------------------

c...  ------------------------------------------------------------------
c...  Subroutine for moving 6x1 tensor to 3x3 tensor
c...  ------------------------------------------------------------------
      subroutine move6to33(a6,a33)
      implicit none
      real*8   a6(6),a33(3,3)

      a33(1,1) = a6(1)
      a33(2,2) = a6(2)
      a33(3,3) = a6(3)
      a33(1,2) = a6(4)
      a33(1,3) = a6(5)
      a33(2,3) = a6(6)
      a33(2,1) = a33(1,2)
      a33(3,1) = a33(1,3)
      a33(3,2) = a33(2,3)

      return
      end

c...  ------------------------------------------------------------------
c...  Subroutine for outer product 3x1 tensor by 3x1 tensor
c...  ------------------------------------------------------------------
      subroutine outer3by3(a3,b3,c33)
      implicit none
      real*8   a3(3),b3(3),c33(3,3)

      c33(1,1) = a3(1)*b3(1)
      c33(2,2) = a3(1)*b3(2)
      c33(3,3) = a3(1)*b3(3)
      c33(1,2) = a3(2)*b3(1)
      c33(1,3) = a3(2)*b3(2)
      c33(2,3) = a3(2)*b3(3)
      c33(2,1) = a3(3)*b3(1)
      c33(3,1) = a3(3)*b3(2)
      c33(3,2) = a3(3)*b3(3)

      return
      end